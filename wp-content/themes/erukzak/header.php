<!DOCTYPE html>
<html <?php language_attributes() ?>>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="yandex-verification" content="799cbaa884dd651b" />
	<?php wp_head() ?>
    <link rel="preload" as="style" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"
          onload="this.rel='stylesheet'"/>
    <link rel="preload" as="font" type="font/woff2" crossorigin
          href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/fonts/fontawesome-webfont.woff2?v=4.3.0"/>
	<?php echo carbon_get_theme_option( 'crb_header_script' ) ?>


</head>
<body <?php body_class( get_query_var( 'terms' ) ? 'tax-catalog' : '' ) ?>>
<nav id="top">
    <div class="container">
        <div class="soc-icon">
            <a href="https://www.facebook.com/erukzakcomua/" rel="nofollow"><span class="fa fa-facebook"></span></a>
            <a href="https://www.instagram.com/erukzakua/" rel="nofollow"><span class="fa fa-instagram"></span></a>
            <a href="https://twitter.com/erukzak" rel="nofollow"><span class="fa fa-twitter"></span></a>
        </div>
        <div class="contact-header">
            <ul>
                <li>
                    <span class="fa fa-map-marker"></span> <?php echo apply_filters( 'the_title', carbon_get_theme_option( 'crb_address' ) ) ?>
                </li>
                <li>
                    <span class="fa fa-clock-o"></span> <?php echo carbon_get_theme_option( 'crb_shedule' ) ?>
                </li>
            </ul>
        </div>
        <div class="top-right">
            <a href="javascript:jivo_api.open();" class="btn btn-primary btn-blue" id="consult">Online</a>
            <a href="#modal-back-call" class="btn btn-primary inline-modal btn-lg"
               data-toggle="modal"><i class="icon"><img
                            src="<?php echo get_template_directory_uri() ?>/img/icons/phone-call.png"
                            alt="icon"></i> <?php esc_html_e( 'Заказать звонок', 'erukzak' ); ?></a>

            <a href="<?php echo the_permalink( 2058 ); ?>" class="quick_signup"><i
                        class="fa fa-lock"></i> <?php esc_html_e( 'Войти', 'erukzak' ); ?></a>

            <div id="language">
				<?php if ( class_exists( 'WPGlobus' ) ) {
					foreach ( WPGlobus::Config()->enabled_languages as $lang ) {

						if ( $lang == WPGlobus::Config()->language ) {
							$class = 'class="active"';
						} else {
							$class = '';
						}
						echo '<a href="' . WPGlobus_Utils::localize_current_url( $lang ) . '" ' . $class . '>' . $lang . '</a>';
					}
				} ?>
            </div>
        </div>
    </div>
</nav>
<div class="container">
    <header>
        <div class="row">
            <div class="col-sm-2">
                <div id="logo">
                    <a <?php if ( ! is_front_page() ): ?>href="<?php echo home_url( '/' ) ?>"<?php endif; ?>><img
                                src="<?php echo get_template_directory_uri() ?>/img/logo.png"
                                title="E-rukzak" alt="E-rukzak"
                                class="img-responsive center-block"></a>
                </div>
            </div>
            <div class="col-sm-5">
                <form method="get" id="search" class="input-group" action="<?php echo esc_url( home_url() ) ?>">
                    <input type="search" name="s" value="<?php echo esc_attr( get_search_query() ) ?>"
                           placeholder="<?php esc_html_e( 'Поиск', 'erukzak' ); ?>"
                           class="form-control" role="search">
                    <div class="smartsearch"></div>
                    <span class="input-group-btn">
        <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
      </span>
                </form>
            </div>
            <div class="col-sm-3 col-xs-6">
                <ul id="header-telephone" class="header-phones">
					<?php
					$phones = carbon_get_theme_option( 'crb_phones' );
					if ( $phones ) {
						foreach ( $phones as $phone ) {
							if ( ! isset( $phone['phone'] ) ) {
								continue;
							}
							echo '<li><a href="tel:' . esc_attr( $phone['phone'] ) . '">' . esc_attr( $phone['phone'] ) . '</a></li>';
						}
					}
					?>
                </ul>
            </div>
            <div class="col-sm-2 col-xs-6">
                <div id="cart-block">
                    <div id="cart">
						<?php fs_cart_widget() ?>
                    </div>
                </div>
            </div>
        </div>
    </header>


    <nav class="navbar navbar-default">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu"
                    aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand hidden-lg hidden-md hidden-sm" href="<?php echo home_url(); ?>/">E-rukzak</a>
        </div>
        <div class="collapse navbar-collapse" id="main-menu">
			<?php
			wp_nav_menu( array(
				'theme_location'  => 'primary',
				'menu'            => '',
				'container'       => 'div',
				'container_class' => '',
				'container_id'    => '',
				'menu_class'      => 'nav navbar-nav',
				'menu_id'         => '',
				'echo'            => true,
				'fallback_cb'     => 'wp_page_menu',
				'before'          => '',
				'after'           => '',
				'link_before'     => '',
				'link_after'      => '',
				'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				'depth'           => 0,
				'walker'          => '',
			) );
			?>
        </div>
    </nav>
</div><!--BOF Color series-->