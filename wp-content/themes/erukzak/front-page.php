<?php get_header() ?>
    <!--EOF Color series-->
    <div class="container">
        <div class="row">
            <div id="content" class="col-sm-12">
                <div class="row">
                    <div class="col-md-3">
                        <div id="banner0">
                            <div class="row left-banners">
                                <div class="col-md-12 col-sm-4">
                                    <div class="item new-items">
                                        <div class="banner-title h3"><?php esc_html_e( 'Новинки', 'erukzak' ); ?></div>
                                        <a href="<?php the_permalink( 3431 ) ?>">
                                            <img
                                                    src="<?php echo get_template_directory_uri() ?>/img/latest-266x140_0.jpg"
                                                    alt="Новинки"
                                                    class="img-responsive center-block"></a>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-4">
                                    <div class="item popular">
                                        <div class="banner-title h3"><?php esc_html_e( 'Популярное', 'erukzak' ); ?></div>
                                        <a href="<?php the_permalink( 3439 ) ?>"><img
                                                    src="<?php echo get_template_directory_uri() ?>/img/popular-266x140_0.jpg"
                                                    alt="Популярное"
                                                    class="img-responsive center-block"></a>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-4">
                                    <div class="item promo">
                                        <div class="banner-title h3"><?php esc_html_e( 'Акции', 'erukzak' ); ?></div>
                                        <a href="<?php the_permalink( 3443 ) ?>"><img
                                                    src="<?php echo get_template_directory_uri() ?>/img/special-266x140_0.jpg"
                                                    alt="Акции"
                                                    class="img-responsive center-block"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div id="slideshow0" class="owl-carousel">
							<?php $slider = get_field( 'home_slider' ); ?>
							<?php if ( $slider ): ?>
								<?php foreach ( $slider as $slide ): ?>
                                    <div>
                                        <div class="item">

                                            <div class="slider-title h3"><?php esc_html_e( 'ВЫБЕРИ СВОЙ РЮКЗАК', 'erukzak' ); ?></div>

											<?php if ( $slide['link']['url'] ): ?>
                                            <a href="<?php echo $slide['link']['url'] ?>">
												<?php endif ?>
												<?php if ( $slide['image'] ) {
													echo '<img src="' . wp_get_attachment_image_url( $slide['image'], 'full' ) . '" class="img-responsive center-block"/>';
												} ?>
												<?php if ( $slide['link']['url'] ): ?>
                                            </a>
										<?php endif ?>
                                        </div>
                                    </div>
								<?php endforeach; ?>
							<?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="widget">
                    <h1 class="widget-heading"><?php the_title() ?></h1>
                    <section class="popular__items">
                        <div class="section-title"><span class="t1"><i
                                        class="icon icon-top"></i> <?php esc_html_e( 'популярное', 'erukzak' ); ?></span>
                        </div>
                        <div class="slider-wrapper">
                            <div class="slick-slider" data-slick='{"slidesToShow":4}'>
								<?php $query = new WP_Query( array(
									'post_type'      => 'product',
									'posts_per_page' => 12,
									'meta_key'       => 'fs_on_bestseller',
									'meta_value'     => '1',
								) ) ?>
								<?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
									<?php get_template_part( 'template-parts/content', get_post_type() ) ?>
								<?php endwhile;
									wp_reset_query(); ?>
								<?php else: ?>
								<?php endif; ?>
                            </div>
                        </div>
                    </section>
                    <section class="new__items">
                        <div class="section-title"><span class="t1"><i
                                        class="icon icon-new"></i> <?php esc_html_e( 'новинки', 'erukzak' ); ?></span>
                        </div>
                        <div class="slider-wrapper">
                            <div class="slick-slider" data-slick='{"slidesToShow":4}'>
								<?php $query = new WP_Query( array(
									'post_type'      => 'product',
									'posts_per_page' => 12,
									'meta_key'       => 'fs_on_novelty',
									'meta_value'     => '1',
								) ) ?>
								<?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
									<?php get_template_part( 'template-parts/content', get_post_type() ) ?>
								<?php endwhile;
									wp_reset_query(); ?>
								<?php else: ?>
								<?php endif; ?>
                            </div>
                        </div>
                    </section>
                    <section class="promo__items">
                        <div class="section-title"><span class="t1"><i
                                        class="icon icon-percentage"></i> <?php esc_html_e( 'Акции', 'erukzak' ); ?></span>
                        </div>
                        <div class="slider-wrapper">
                            <div class="slick-slider" data-slick='{"slidesToShow":4}'>
								<?php $query = new WP_Query( array(
									'post_type'      => 'product',
									'posts_per_page' => 12,
									'meta_key'       => 'fs_on_promotion',
									'meta_value'     => '1',
								) ) ?>
								<?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
									<?php get_template_part( 'template-parts/content', get_post_type() ) ?>
								<?php endwhile;
									wp_reset_query(); ?>
								<?php else: ?>
								<?php endif; ?>
                            </div>
                        </div>
                    </section>

                </div>
            </div>
        </div>
    </div>
    <section class="reviews section section-dark">
        <div class="container">
            <div class="h2"><?php esc_html_e( 'отзывы', 'erukzak' ); ?></div>
            <div class="slider-wrapper">
                <div class="slick-slider" data-slick='{"slidesToShow":3}'>
					<?php $query = new WP_Query( array( 'post_type' => 'reviews' ) ) ?>
					<?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
						<?php get_template_part( 'template-parts/content', get_post_type() ) ?>
					<?php endwhile;
						wp_reset_query(); ?>
					<?php else: ?>
					<?php endif; ?>
                </div>
            </div>
            <div class="section-buttons"><a href="<?php the_permalink( 1892 ) ?>"
                                            class="bts"><?php esc_html_e( 'Смотреть все отзывы', 'erukzak' ); ?></a> <a
                        data-toggle="modal" href="#modal-review"
                        class="bts bts-orange"><?php esc_html_e( 'Оставить свой отзыв', 'erukzak' ); ?></a></div>

        </div>
    </section>
<?php if ( get_field( 'map' ) ): ?>
    <section class="section">
        <div class="container">
            <div class="h2"><?php esc_html_e( 'мы в разных городах украины', 'erukzak' ); ?></div>
			<?php the_field( 'map' ) ?>
        </div>
    </section>
<?php endif ?>

    <section class="section section-padding">
        <div class="container">
            <div class="h2"><?php esc_html_e( 'Огромный выбор рюкзаков', 'erukzak' ); ?></div>
            <div class="row">
				<?php
				global $post;
				$field = get_field( 'selection_backpacks', $post->ID );

				if ( have_rows( 'selection_backpacks', $post->ID ) ): ?>
					<?php
					$increment = 0;
					while ( have_rows( 'selection_backpacks', $post->ID ) ): the_row(); ?>
                        <div class="col-lg-3">
                            <div class="main-filter">
                                <div class="main-filter__title">
									<?php the_sub_field( 'title' ); ?>:
                                </div>
                                <ul id="main-filter-<?php echo $increment ?>" data-max-height="134" data-block-hide="1">
									<?php
									$terms = get_sub_field( 'link' );
									if ( $terms ) {
										foreach ( $terms as $term ) { ?>
											<?php
											$term_id = absint( $term['target_page'] );
											$title   = apply_filters( 'the_title', get_term_field( 'name', $term_id ) );
											?>
											<?php if ( apply_filters( 'the_title', $term['type'] ) == 'text' ): ?>
                                                <li>
                                                    <a href="<?php echo esc_url( get_term_link( intval( $term['target_page'] ) ) ) ?>"><?php echo $title?></a>
                                                </li>
											<?php else: ?>
                                                <a href="<?php echo esc_url( get_term_link( intval( $term['target_page'] ) ) ) ?>"
                                                   title="<?php echo apply_filters( 'the_title', $term['title'] ); ?>"
                                                   class="attr-color"
                                                   style="background-color: <?php echo esc_attr( $term['color'] ) ?>;"></a>
											<?php endif ?>
										<?php }
									}

									?>
                                </ul>
                                <a href="#main-filter-<?php echo $increment ?>" class="main-filter__show-hide"
                                   data-target="#main-filter-<?php echo $increment ?>"
                                   data-action="block-hide"><?php esc_html_e('Смотреть все','erukzak'); ?> <i
                                            class="icon icon-arrow-right"></i></a>

                            </div>
                        </div>
						<?php
						$increment ++;
					endwhile; ?>
				<?php endif; ?>
            </div>

        </div>
    </section>
    <section class="section section-dark page__content">
        <div class="container">
            <h2><?php the_title() ?></h2>
            <div class="page-text">
                <div class="nice-scroll">
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<?php the_content() ?>
					<?php endwhile; ?>
					<?php else: ?>
					<?php endif; ?>
                </div>
            </div>

        </div>
    </section>
    <section class="company__address">
		<?php echo do_shortcode( '[google_map_easy id="2"]' ) ?>
        <div class="map-info">
			<?php the_field( 'home_map_address' ) ?>
        </div>
    </section>
<?php get_footer();