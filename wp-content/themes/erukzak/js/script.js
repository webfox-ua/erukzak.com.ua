/**
 * Created by VaLeXaR on 11.12.2015.
 */
(function($)
{
    $(document).ready(function() {

        $('.bf-cur-symb .ndash').click(function(){
            BrainyFilter.reset();
        });

        $('input:radio').on('change', function(){
            $('input:radio').trigger('refresh');
        });


        $('input:checkbox, input:radio').styler();

        //$('.bf-sliding').mCustomScrollbar({
        //    theme:"dark-2",
        //    scrollButtons:{ enable: false }
        //});

        $('.inline-modal').magnificPopup({
            type: 'inline',
            removalDelay: 500,
            mainClass: 'mfp-zoom-in',
            callbacks: {
                open: function () {
                    $('input[type="tel"], input[name="phone"]').mask("+38 (099) 999-99-99");
                    $('input[type="number"]:not(.rating)').number();
                }
            }
        });

        $('input[type="tel"], input[name="phone"]').mask("+38 (099) 999-99-99");

        $('#consult').click(function () {
            jivo_api.open();
            return false;
        });

    })
})(jQuery);