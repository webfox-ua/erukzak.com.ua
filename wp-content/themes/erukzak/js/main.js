var $ = jQuery;
jQuery(document).ready(function ($) {

    
    $('[data-toggle="tooltip"]').tooltip()

    $("[type=tel]").mask("+38 (999)-999-99-99", {placeholder: "+38 (___)-___-__-__"});

    $(document).on('fs_add_to_cart', function (event) {
        $('#modal-cart').modal('show');
    });


    $(".slick-slider").each(function (index, value) {
        $(this).slick({
            nextArrow: "<button class=\"slick-next\"></button>",
            prevArrow: "<button class=\"slick-prev\"></button>"
        });

    });

    $(".product-gallery").each(function (index, value) {
        $(this).slick({
            nextArrow: "<button class=\"slick-next\"></button>",
            prevArrow: "<button class=\"slick-prev\"></button>"
        });

    });
    $('.select').each(function () {
        var $this = $(this),
            selectOption = $this.find('option'),
            selectOptionLength = selectOption.length,
            selectedOption = selectOption.filter(':selected'),
            dur = 300;

        $this.hide();
        $this.wrap('<div class="select"></div>');
        $('<div>', {
            class: 'select__gap',
            text: selectOption[0]['label']
        }).insertAfter($this);

        var selectGap = $this.next('.select__gap'),
            caret = selectGap.find('.caret');
        $('<ul>', {
            class: 'select__list'
        }).insertAfter(selectGap);

        var selectList = selectGap.next('.select__list');
        // Add li - option items
        for (var i = 0; i < selectOptionLength; i++) {
            $('<li>', {
                class: 'select__item',
                html: $('<span>', {
                    text: selectOption.eq(i).text()
                })
            })
                .attr('data-value', selectOption.eq(i).val())
                .appendTo(selectList);
        }
        var selectItem = selectList.find('li');

        selectList.slideUp(0);
        selectGap.on('click', function () {
            if (!$(this).hasClass('on')) {
                $(this).addClass('on');
                selectList.slideDown(dur);

                selectItem.on('click', function () {
                    var chooseItem = $(this).data('value');

                    $this.val(chooseItem).attr('selected', 'selected');
                    selectGap.text($(this).find('span').text());

                    selectList.slideUp(dur);
                    selectGap.removeClass('on');
                    $this.change();
                });

            } else {
                $(this).removeClass('on');
                selectList.slideUp(dur);
            }
        });


    });


    //Скролл вверх
    $(function () {
        $.fn.scrollToTop = function () {
            $(this).hide().removeAttr("href");
            if ($(window).scrollTop() != "0") {
                $(this).fadeIn("slow")
            }
            var scrollDiv = $(this);
            $(window).scroll(function () {
                if ($(window).scrollTop() == "0") {
                    $(scrollDiv).fadeOut("slow")
                } else {
                    $(scrollDiv).fadeIn("slow")
                }
            });
            $(this).click(function () {
                $("html, body").animate({scrollTop: 0}, "slow")
            })
        }
    });
    $(function () {
        $("#toTop").scrollToTop();
    });


    $(".nice-scroll").niceScroll({
        autohidemode: false,
        cursorcolor: "#009ba0",
        cursorborder: "0px solid #fff",
        background: "#fff",
        cursorwidth: "7px",
        iframeautoresize: true
    });

    $(".fs-attr-filter").niceScroll({
        autohidemode: false,
        cursorcolor: "#009ba0",
        cursorborder: "0px solid #fff",
        background: "#f0f0f0",
        cursorwidth: "7px",
    });


    $('section').on('resize',function (event) {
        $(".nice-scroll").getNiceScroll().resize();
    });


    $(document).on('click', '.widgettitle', function (event) {
        $(this).toggleClass('active').next().slideToggle(function () {
            $(".fs-attr-filter").getNiceScroll().resize();

        });


    });
    $(document).on('click', '.thumbnail', function (event) {
        event.preventDefault();
        console.log($(this).attr("href"));
        $(".main-img img").attr("src", $(this).attr("href"));
    });
    /*   $('a.title').cluetip({splitTitle: '|'});
       $('ol.rounded a:eq(0)').cluetip({
           splitTitle: '|',
           dropShadow: false,
           cluetipClass: 'rounded',
           showtitle: false
       });
       $('ol.rounded a:eq(1)').cluetip({
           cluetipClass: 'rounded',
           dropShadow: false,
           showtitle: false,
           positionBy: 'mouse'
       });
       $('ol.rounded a:eq(2)').cluetip({
           cluetipClass: 'rounded',
           dropShadow: false,
           showtitle: false,
           positionBy: 'bottomTop',
           topOffset: 70
       });
       $('ol.rounded a:eq(3)').cluetip({
           cluetipClass: 'rounded',
           dropShadow: false,
           sticky: true,
           ajaxCache: false,
           arrows: true
       });
       $('ol.rounded a:eq(4)').cluetip({cluetipClass: 'rounded', dropShadow: false});*/

    $("[data-block-hide=\"1\"]").each(function (index, value) {
        let el = $(this);

        // Скрываем кнопку "показать все" если высота списка меньше указанной высоты в data-height
        if ($(this).height() < el.data('max-height')) {
            $('[data-target="#' + el.attr('id') + '"]').fadeOut();
        }

        $(this)
            .attr('data-height', $(this).height())
            .css({
                "height": el.data('max-height') + 'px',
                "overflow": 'hidden',
            });
    });

    $(document).on('click', '[data-action="block-hide"]', function (event) {
        event.preventDefault();

        let elThis = $(this);
        let el = $($(this).data('target'));
        let duration = 500;

        if (el.hasClass('active') == false) {
            el.animate({
                height: el.data('height') + elThis.height()
            }, duration).addClass('active');

            elThis.addClass('active');
        } else {
            el.animate({
                height: el.data('max-height')
            }, duration).removeClass('active');

            elThis.removeClass('active');
        }

    });

});


var wait;
var searchinput;


$('#one_more_msg_href0').live('click', function () {
    $('#wsf_enquiry_send_result0').hide('fast', function () {
        $('#sidebar_feedback_div0').show('fast');
    });
});


$('#slideshow0').owlCarousel({
    autoPlay: true,
    singleItem: true,
    navigation: true,
    navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>'],
    pagination: true
});

$("a.color-option").click(function (event) {
    $this = $(this);

    $this.parent().find('a.color-option').removeClass('color-active');
    $this.addClass('color-active');

    $this.parent().find('select').val($this.attr('option-value'));

    $('#' + $this.attr('option-text-id')).html($this.attr('title'));

    //option redux
    if (typeof updatePx == 'function') {
        updatePx();
    }

    //option boost
    if (typeof obUpdate == 'function') {
        obUpdate($($this.parent().find('select option:selected')), useSwatch);
    }
    event.preventDefault();
});

$('#ne_subscribe0 .ne_submit').click(function (e) {
    e.preventDefault();

    var list = $('#ne_subscribe0 .ne_subscribe_list:checked').map(function (i, n) {
        return $(n).val();
    }).get();

    $.post("https://erukzak.com.ua/index.php?route=module/ne/subscribe&box=1", {
        email: $('#ne_subscribe0 input[name="ne_email"]').val(),
        'list[]': list
    }, function (data) {
        if (data) {
            if (data.type == 'success') {
                $('#ne_subscribe0 input[type="text"]').val('');
                $('#ne_subscribe0 .ne_subscribe_list').removeAttr('checked');
            }
            $("#ne_subscribe0 div." + data.type).remove();
            $('#ne_subscribe0').prepend('<div class="' + data.type + '">' + data.message + '</div>');
            $("#ne_subscribe0 div." + data.type).delay(3000).slideUp(400, function () {
                $(this).remove();
            });
        } else {
            $('#ne_subscribe0 input[type="text"]:first').focus();
        }
    }, "json");
});

$(document).ready(function () {


    $('.main-img img').elevateZoom({
        zoomWindowFadeIn: 500,
        zoomWindowFadeOut: 500,
        lensFadeIn: 500,
        lensFadeOut: 500,
        gallery: 'gallery_additional',
        galleryActiveClass: "active"
    });

    $(".main-img img").bind("click", function (e) {
        var ez = $('.main-img img').data('elevateZoom');
        var images = ez.getGalleryList(),
            items = [];
        items.push({
            src: $(this).data('zoom-image')
        });
        $(images).each(function (i, obj) {
            items.push({
                src: obj.href
            });
        });
        $.magnificPopup.open({
            type: 'image',
            mainClass: 'mfp-zoom-in',
            removalDelay: 500,
            items: items,
            gallery: {
                enabled: true
            }
        });
        return false;
    });

    $('#gallery_additional')
        .on('jcarousel:reload jcarousel:create', function () {
            var carousel = $(this),
                width = carousel.innerWidth(),
                height = $('.main-img').innerHeight();
            carousel.jcarousel('items').css('width', Math.ceil(width - 1) + 'px');
            carousel.height(height - 70);
        })
        .jcarousel({
            wrap: 'circular',
            vertical: true,
            scroll: 1
        });

    $('.jcarousel-control-prev')
        .jcarouselControl({
            target: '-=1'
        });

    $('.jcarousel-control-next')
        .jcarouselControl({
            target: '+=1'
        });

    var categoryNavHeight = 100;
    var showHideBtn = $('<button class="bts bts-outline bts-block bts-sm" data-action="block-hide"  data-target="#menu-kategorii-tovara">Смотреть все</button>');

    $("#menu-kategorii-tovara").each(function (index, value) {
        let el = $(this);
        el.data('max-height', categoryNavHeight);

        // Скрываем кнопку "показать все" если высота списка меньше указанной высоты в data-height
        if ($(this).height() < categoryNavHeight) {
            $('[data-target="#' + el.attr('id') + '"]').fadeOut();
        }

        $(this)
            .attr('data-height', $(this).height())
            .css({
                "height": el.data('max-height') + 'px',
                "overflow": 'hidden',
            });


        $(this).after(showHideBtn);
    });

    $(document).on('click', '[data-action="block-hide"]', function (event) {
        event.preventDefault();

        let elThis = $(this);
        let el = showHideBtn;
        let duration = 800;

        if (el.hasClass('active') == false) {
            el.addClass('active')
                .animate({
                    height: el.data('height') + elThis.height()
                }, duration);

            elThis.addClass('active');
        } else {
            el.removeClass('active')
                .animate({
                    height: el.data('max-height')
                }, duration);

            elThis.removeClass('active');
        }

    });


})


