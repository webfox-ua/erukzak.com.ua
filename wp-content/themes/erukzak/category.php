<?php get_header() ?>
  <div class="container">

    <ul class="breadcrumb" typeof="BreadcrumbList" vocab="https://schema.org/">
		<?php if ( function_exists( 'bcn_display_list' ) ) {
			bcn_display_list();
		} ?>
    </ul>

    <div class="row">
      <div id="content" class="col-md-12">
        <h1><?php single_cat_title() ?></h1>
        <div id="blogCatArticles" class=" ">
          <div class="row">
			  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				  <?php get_template_part( 'template-parts/content', get_post_type() ) ?>
			  <?php endwhile; ?>
			  <?php else: ?>
			  <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php get_footer();