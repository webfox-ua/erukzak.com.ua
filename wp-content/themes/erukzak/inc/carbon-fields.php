<?php
/**
 * Created by PhpStorm.
 * User: karak
 * Date: 12.08.2018
 * Time: 15:14
 */

use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action( 'carbon_fields_register_fields', 'crb_attach_theme_options' );
function crb_attach_theme_options() {
	Container::make( 'theme_options', 'Настройки темы' )
	         ->add_tab( __( 'Контакты' ), array(
		         Field::make( 'text', 'crb_address', 'Адрес' ),
		         Field::make( 'text', 'crb_shedule', 'Время работы' ),
		         Field::make( 'complex', 'crb_phones', 'Телефоны' )
		              ->add_fields( array(
			              Field::make( 'text', 'phone', 'Телефон' ),
		              ) )
	         ) )
	         ->add_tab( __( 'Социальные сети' ), array(
		         Field::make( 'text', 'crb_facebook_link' ),
		         Field::make( 'text', 'crb_twitter_link' ),
	         ) )
	         ->add_tab( __( 'Настройки футера' ), array(
		         Field::make( 'header_scripts', 'crb_header_script' ),
		         Field::make( 'footer_scripts', 'crb_footer_script' ),
		         Field::make( 'text', 'crb_copywrite', 'Копирайт, права' ),
		         Field::make( 'text', 'crb_developer_link', 'Ссылка разработчика' ),
	         ))
	         ->add_tab( __( 'Настройки страницы товара' ), array(

		         Field::make( 'rich_text', 'crb_product_advantages', 'Наши преимущества' ),
		         Field::make( 'rich_text', 'crb_product_shipping', 'Доставка и оплата' ),

	         ) );


}

