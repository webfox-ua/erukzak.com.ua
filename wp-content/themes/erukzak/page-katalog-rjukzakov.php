<?php get_header() ?>
    <div class="container">

        <ul class="breadcrumb" typeof="BreadcrumbList" vocab="https://schema.org/">
			<?php if ( function_exists( 'bcn_display_list' ) ) {
				bcn_display_list();
			} ?>
        </ul>

        <div class="row">
            <div id="content" class="col-md-12">
                <h1><?php the_title() ?></h1>
                <div class="row">
					<?php
					$args  = array(
						'taxonomy'     => 'catalog',
						'hide_empty'   => true,
						'include'      => array(),
						'exclude'      => array(),
						'fields'       => 'all',
						'parent'       => 0,
						'hierarchical' => true,
						'meta_query'   => [
							'relation'   => 'OR',
							[
								[
									'key'     => 'exclude_catalog',
									'compare' => 'EXISTS',
								],
								[
									'key'     => 'exclude_catalog',
									'compare' => '!=',
									'value'   => '1',
								]
							],
							[
								[
									'key'     => 'exclude_catalog',
									'compare' => 'NOT EXISTS',
								]
							]
						]
					);
					$terms = get_terms( $args );

					foreach ( $terms as $term ): ?>
                        <div class="col-md-4">
                            <div class="category-item">
                                <a href="<?php echo esc_attr( get_term_link( $term ) ) ?>">
									<?php if ( $atach_id = get_term_meta( $term->term_id, '_thumbnail_id', 1 ) ) {
										$image = wp_get_attachment_image_url( $atach_id, 'full', false, array( 'class' => 'img-responsive center-block' ) );
									} else {
										$image = '/wp-content/plugins/f-shop/assets/img/no-image.png';
									} ?>
                                    <figure
                                            style="background:  url(<?php echo esc_attr( $image ) ?>) center center no-repeat;"
                                            class="cat-image"></figure>
                                </a>
                                <div class="name"><a
                                            href="<?php echo esc_attr( get_term_link( $term ) ) ?>"><?php echo $term->name ?></a>
                                </div>
                            </div>
                        </div>
					<?php endforeach; ?>

                </div>
            </div>
        </div>
    </div>

<?php if ( get_the_content()): ?>
    <section class="section section-dark page__content">
        <div class="container">
            <!--  <h2><?php /*the_title() */ ?></h2>-->
            <div class="page-text">
                <div class="nice-scroll">
					<?php echo get_the_content() ?>
                </div>
            </div>

        </div>
    </section>
<?php endif ?>
<?php get_footer();