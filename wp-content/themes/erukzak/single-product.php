<?php use FS\FS_Config;

get_header() ?>
    <div class="container">

        <ul class="breadcrumb" typeof="BreadcrumbList" vocab="https://schema.org/">
			<?php if ( function_exists( 'bcn_display_list' ) ) {
				bcn_display_list();
			} ?>
        </ul>

        <div class="row">
            <div id="content" class="col-md-12">
				<?php if ( have_posts() ) : while ( have_posts() ) :
				the_post();
				?>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-md-12 clearfix">
                                <div class="pull-left"><?php previous_post_link( '%link', '<span>' . esc_html__( 'Предыдущий товар', 'erukzak' ) . '</span>', false ); ?></div>
                                <div class="pull-right">  <?php next_post_link( '%link', '<span>' . esc_html__( 'Следующий товар', 'erukzak' ) . '</span>', false ); ?></div>
                            </div>
                            <div class="col-md-12"> <?php fs_lightslider( 0, array(
									'vertical'     => true,
									'thumbItem'    => 6,
									"gallery_args" => array(
										"attachments" => true
									)
								) ) ?></div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div id="product">
                            <h1 class="product-name">
								<?php the_title() ?>
                            </h1>

                            <div class="product__buttons">
                                <div class="row">
                                    <div class="col-xl-3 col-lg-6">
										<?php fs_the_price() ?>

                                    </div>
                                    <div class="col-xl-3 col-lg-6">
	                                    <?php fs_base_price(); ?>
                                    </div>
                                    <div class="col-xl-3 col-lg-6">
										<?php fs_product_code( 0, 'Артикул: %s' ); ?>
                                    </div>
                                    <div class="col-xl-3 col-lg-6">
                                        <div class="rating-block">
                                            <label for="input-rating"><?php esc_html_e( 'Рейтинг', 'erukzak' ); ?>
                                                :</label>
                                            <div class="star-rating rating-xxs">
												<?php do_action( 'fs_product_rating' ) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="quantity-block">
											<?php do_action( 'fs_quantity_product', 0, array(
													'position'      => '%input% %pluss%  %minus%',
													// позиция элементов
													'wrapper'       => 'div',
													// html элемент обёртки
													'wrapper_class' => 'fs-number',
													// html class элемента обёртки
													'pluss_class'   => 'fs-number-arrow fs-number-up',
													// html class кнопки увеличения к-ва
													'pluss_content' => 'Up',
													// внутреннее содержимое кнопки увеличения к-ва
													'minus_class'   => 'fs-number-arrow fs-number-down',
													// html class кнопки уменьшения к-ва
													'minus_content' => 'Down',
													// внутреннее содержимое кнопки уменьшения к-ва
													'input_class'   => 'form-control fs-number-element',
													// html class поля типа input
													'echo'          => true
													// выводить или возвращать (актуально если использоввать не хук do_action() а функцию fs_quantity_product()
												)
											); ?>

                                        </div>
                                    </div>
                                    <div class="col">
										<?php fs_add_to_cart( 0, '<i class="icon icon-cart-lg"></i> ' . esc_html__( 'Купить', 'erukzak' ), array( 'class' => 'btn btn-primary btn-lg btn-block' ) ) ?>

                                    </div>
                                    <div class="col"> <?php  fs_add_to_wishlist(0,'<i class="fas fa-heart"></i>',[
                                            'class'=>'btn btn-lg btn-default btn-wishlist'
                                        ]);  ?></div>
                                    <div class="col">
                                        <a href="#quick-buy" class="btn btn-default btn-block inline-modal btn-lg"
                                           data-toggle="modal"><?php esc_html_e( 'купить в
                                            один клик', 'erukzak' ); ?></a>
                                        <div class="modal fade" id="quick-buy">
                                            <div class="modal-dialog">
                                                <div class="modal-content mfp-with-anim">
                                                    <div class="widget-heading"><span class="sprite quick-buy"></span>
														<?php esc_html_e( 'Купить в один клик', 'erukzak' ); ?>
                                                    </div>
													<?php
													if ( get_locale() == 'uk' ) {
														echo do_shortcode( '[contact-form-7 id="5612" title="Быстрая покупка_ua" html_class="form-horizontal"]' );
													} else {
														echo do_shortcode( '[contact-form-7 id="5610" title="Быстрая покупка" html_class="form-horizontal"]' );
													}
													?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="attribute-header"><?php esc_html_e( 'Характеристики товара', 'erukzak' ); ?></div>
							<?php do_action( 'fs_the_atts_list' ) ?>
							<?php
							$up_sells = fs_get_up_sells();
							if ( $up_sells->have_posts() ) : ?>
                                <div class="attribute-header"><?php esc_html_e( 'Товар в других цветах', 'erukzak' ); ?></div>
                                <ul class="upsells-list">
									<?php while ( $up_sells->have_posts() ): ?>
										<?php $up_sells->the_post();
										$attributes = wp_get_object_terms( get_the_ID(), FS_Config::get_data( 'features_taxonomy' ), [ 'parent' => 1628 ] );

										if ( ! $attributes ) {
											continue;
										}
										$attributes      = array_shift( $attributes );
										$attribute_type  = get_term_meta( $attributes->term_id, 'fs_att_type', 1 );
										$attribute_value = get_term_meta( $attributes->term_id, 'fs_att_' . $attribute_type . '_value', 1 );

										if ( ! $attribute_value ) {
											continue;
										}
										?>
                                        <li>
                                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"
                                               style="background-color:<?php echo esc_attr( $attribute_value ); ?>;"></a>
                                        </li>
									<?php endwhile; ?>
									<?php wp_reset_query(); ?>
                                </ul>

							<?php endif; ?>
							<?php echo do_shortcode( '[TheChamp-Sharing]' ) ?>
                        </div>
                    </div>
                </div>
                <ul class="nav nav-tabs product-tabs">
                    <li class="active"><a href="#tab-description"
                                          data-toggle="tab"><?php esc_html_e( 'Описание', 'erukzak' ); ?></a></li>


                    <li><a href="#tab-review" data-toggle="tab"><?php esc_html_e( 'Отзывы', 'erukzak' ); ?>
                            (<?php echo get_comments_number( $post->ID ) ?>
                            )</a></li>


                    <li><a href="#tab-delivery" data-toggle="tab"><?php _e( 'Доставка и оплата', 'erukzak' ); ?></a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-description">
						<?php the_content() ?>
                    </div>
                    <div class="tab-pane" id="tab-review">
                        <div class="h3"
                             style="    max-width: 950px; margin: 20px auto;"><?php esc_html_e( 'Оставить отзыв', 'erukzak' ); ?></div>
						<?php // If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif; ?>
                    </div>
                    <div class="tab-pane" id="tab-delivery">
						<?php echo apply_filters( 'the_content', carbon_get_theme_option( 'crb_product_shipping' ) ); ?>
                    </div>
                </div>
            </div>

			<?php endwhile; ?>
			<?php else: ?>
			<?php endif; ?>
        </div>
    </div>



<?php $related = fs_get_related_products( 0, array( 'limit' => 12 ) ) ?>
<?php if ( $related->have_posts() ): ?>
    <section class="section">
        <div class="container">
            <div class="h3"><?php esc_html_e( 'Похожие товары', 'erukzak' ); ?></div>
            <div class="slider-wrapper">
                <div class="slick-slider" data-slick='{"slidesToShow":4}'>

					<?php
					while ( $related->have_posts() ) {
						$related->the_post();
						get_template_part( 'template-parts/content', get_post_type() );
					}
					?>
                </div>
            </div>
        </div>
    </section>
<?php endif ?>
<?php get_footer();