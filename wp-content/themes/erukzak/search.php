<?php get_header() ?>
  <div class="container">
    <ul class="breadcrumb" typeof="BreadcrumbList" vocab="https://schema.org/">
		<?php if ( function_exists( 'bcn_display_list' ) ) {
			bcn_display_list();
		} ?>
    </ul>
    <h1>Поиск по: &laquo;<?php echo get_search_query() ?>&raquo;</h1>
    <div class="row">
      <div id="content" class="col-sm-12">
        <div class="filters__top">
          <div class="order-filter">
            <span class="filter-text">Сортировать по:</span> <?php do_action( 'fs_types_sort_filter' ) ?></div>
          <div class="perpage_filter"><span
              class="filter-text">Выводить по:</span> <?php do_action( 'fs_per_page_filter' ) ?></div>

        </div>
        <div class="row product-list">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

              <div class="product-layout col-sm-6 col-md-3">
				  <?php get_template_part( 'template-parts/content', get_post_type() ) ?>
              </div>
			<?php endwhile; ?>
			<?php else: ?>
          <p class="col-md-12 alert alert-warning" style="margin-left: 15px;">Не найдено товаров по вашему запросу</p>
			<?php endif; ?>
        </div>
        <div class="taxonomy__pagination">

			<?php if ( $wp_query->max_num_pages>1 ): ?>
              <button class="btn">показать еще</button>
			<?php endif ?>
			<?php
			$args = array(
				'prev_next' => true,
				'prev_text' => __( '&laquo;' ),
				'next_text' => __( '&raquo;' ),
				'type'      => 'list',

			);
			echo str_replace( 'page-numbers', 'pagination', paginate_links( $args ) );
			?>
        </div>

      </div>
    </div><!--row-->
  </div><!--container-->
<?php get_footer();