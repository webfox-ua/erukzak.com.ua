<?php
get_header();
$city_categories = get_field( 'city_cat', 'term_' . get_queried_object_id() );
?>
    <div class="container">
        <ul class="breadcrumb" typeof="BreadcrumbList" vocab="https://schema.org/">
			<?php if ( function_exists( 'bcn_display_list' ) ) {
				bcn_display_list();
			} ?>
        </ul>
        <h1><?php custom_taxonomy_title() ?></h1>
        <div class="row">
            <aside id="column-left" class="col-sm-3 hidden-xs taxonomy-sidebar">
				<?php
				if ( function_exists( 'dynamic_sidebar' ) ) {
					dynamic_sidebar( 'category_sidebar' );
				}
				?>
            </aside>
            <div id="content" class="col-sm-9">
				<?php
				$term_subcategories = get_field( 'term_subcategories', 'term_' . get_queried_object_id() );
				$args               = array(
					'taxonomy'     => 'catalog',
					'hide_empty'   => false,
					'include'      => ! empty( $term_subcategories ) && is_array( $term_subcategories ) ? $term_subcategories : [],
					'exclude'      => array(),
					'fields'       => 'all',
					'parent'       => get_queried_object_id(),
					'hierarchical' => true,


				);
				$terms              = get_terms( $args ); ?>
				<?php if ( ! empty( $term_subcategories ) && $terms && ! get_query_var( 'terms' ) ): ?>

                    <div class="slider-wrapper">
                        <div class="slick-slider taxonomy-slider-top" data-slick='{"slidesToShow":6}'>
							<?php
							foreach ( $terms as $term ): ?>
								<?php if ( get_term_meta( $term->term_id, 'city_name', 1 ) ) {
									continue;
								} ?>
                                <div>
                                    <div class="taxonomy__top-item">
                                        <figure>
                                            <a href="<?php echo esc_url( get_term_link( $term ) ) ?>">
												<?php $thumb = get_term_meta( $term->term_id, '_thumbnail_id', 1 );
												if ( $thumb ) {
													echo wp_get_attachment_image( $thumb, 'full' );
												} else {
													echo '<img src="' . get_template_directory_uri() . '/img/no-image.svg" width="100" alt="no image">';
												}

												?>
                                            </a>
                                        </figure>
                                        <a href="<?php echo esc_url( get_term_link( $term ) ) ?>"
                                           class="name"><?php echo fs_get_term_meta( '_alternative_name', $term->term_id ) ? fs_get_term_meta( '_alternative_name', $term->term_id ) : $term->name ?></a>
                                    </div>
                                </div>
							<?php endforeach; ?>
                        </div>
                    </div>
				<?php endif ?>
				<?php if ( have_posts() ): ?>
                    <div class="filters__top">
                        <div class="order-filter">
                            <span class="filter-text"><?php esc_html_e( 'Сортировать по', 'erukzak' ); ?>:</span> <?php do_action( 'fs_types_sort_filter', [ 'class' => 'select' ] ) ?>
                        </div>
                        <div class="perpage_filter"><span
                                    class="filter-text"><?php esc_html_e( 'Выводить по', 'erukzak' ); ?>:</span> <?php do_action( 'fs_per_page_filter', [ 'class' => 'select' ] ) ?>
                        </div>

                    </div>
				<?php endif ?>

                <div class="row product-list">
					<?php
					if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                        <div class="product-layout col-sm-6 col-md-4">
							<?php get_template_part( 'template-parts/content', get_post_type() ) ?>
                        </div>
					<?php endwhile; ?>
					<?php else: ?>
                        <p class="col-md-12 alert alert-warning"
                           style="margin-left: 15px;"><?php esc_html_e( 'Не найдено товаров по вашему запросу', 'erukzak' ); ?></p>
					<?php endif; ?>
                </div>
                <div class="taxonomy__pagination">

					<?php if ( $wp_query->max_num_pages > 1 ): ?>
                        <button class="btn"><?php esc_html_e( 'показать еще', 'erukzak' ); ?></button>
					<?php endif ?>
					<?php
					$args = array(
						'prev_next' => true,
						'prev_text' => __( '&laquo;' ),
						'next_text' => __( '&raquo;' ),
						'type'      => 'list',

					);
					echo str_replace( 'page-numbers', 'pagination', paginate_links( $args ) );
					?>
                </div>

				<?php if ( ! empty( $city_categories ) ): ?>
                    <section class="section">
                        <div class="h2"><?php if ( $city_title = get_field( 'city_title', 'term_' . get_queried_object_id() ) ) {
								echo $city_title;
							} else {
								esc_html_e( 'мы в разных городах украины', 'erukzak' );
							} ?></div>
                        <div class="row">
                            <div class="col-lg-5">
                                <ul class="map-city">
									<?php foreach ( $city_categories as $city_category ): ?>
                                        <li>
                                            <a href="<?php echo esc_url( get_term_link( $city_category ) ); ?>"><?php echo get_field( 'city_name', 'term_' . $city_category->term_id ) ?></a>
                                        </li>
									<?php endforeach; ?>
                                </ul>

                            </div>
                            <div class="col-lg-7">
                                <img src="/wp-content/themes/erukzak/img/map-uk.jpg" alt="карта Украины">
                            </div>
                        </div>

                    </section>
				<?php endif ?>

				<?php
				global $post;
				$field = get_field( 'selection_backpacks', 'term_' . get_queried_object_id() );
				if ( have_rows( 'selection_backpacks', 'term_' . get_queried_object_id() ) ): ?>

                    <section class="section pt-0 pb-0">
                        <div class="h2"><?php esc_html_e('Огромный выбор рюкзаков','erukzak'); ?></div>
                        <div class="row">
							<?php
							$increment = 0;
							while ( have_rows( 'selection_backpacks', 'term_' . get_queried_object_id() ) ):
								the_row();
								?>
                                <div class="col-lg-3">
                                    <div class="main-filter">
                                        <div class="main-filter__title">
											<?php the_sub_field( 'title' ); ?>:
                                        </div>
                                        <ul id="main-filter-<?php echo $increment ?>" data-max-height="134"
                                            data-block-hide="1">
											<?php
											$terms = get_sub_field( 'link' );
											if ( $terms ) {
												foreach ( $terms as $term ) { ?>

													<?php
													$term_id = absint( $term['target_page'] );
													$title   =  apply_filters( 'the_title', get_term_field( 'name', $term_id ) );

													if ( apply_filters( 'the_title', $term['type'] ) == 'text' ): ?>
                                                        <li>
                                                            <a href="<?php echo esc_url( get_term_link( intval( $term['target_page'] ) ) ) ?>"><?php echo $title; ?></a>
                                                        </li>
													<?php else: ?>
                                                        <a href="<?php echo esc_url( get_term_link( intval( $term['target_page'] ) ) ) ?>"
                                                           title="<?php echo apply_filters( 'the_title', $term['title'] ); ?>"
                                                           class="attr-color"
                                                           style="background-color: <?php echo esc_attr( $term['color'] ) ?>;"></a>
													<?php endif ?>
												<?php }
											}

											?>
                                        </ul>
                                        <a href="#main-filter-<?php echo $increment ?>" class="main-filter__show-hide"
                                           data-target="#main-filter-<?php echo $increment ?>"
                                           data-action="block-hide"><?php esc_html_e('Смотреть все','erukzak'); ?> <i
                                                    class="icon icon-arrow-right"></i></a>

                                    </div>
                                </div>
								<?php
								$increment ++;
							endwhile; ?>

                        </div>
                    </section>
				<?php endif; ?>

				<?php
				$default_meta_key = '_content';
				$meta_key         = get_locale() == 'ru_RU' ? '_content' : '_content__' . get_locale();
				$content          = get_term_meta( get_queried_object_id(), $meta_key, 1 )
					? get_term_meta( get_queried_object_id(), $meta_key, 1 ) :
					get_term_meta( get_queried_object_id(), $default_meta_key, 1 );
				if ( $content ): ?>
                    <div class="category-description">
						<?php echo apply_filters( 'the_content', $content ); ?>
                    </div>
				<?php endif ?>


            </div>
        </div><!--row-->
    </div><!--container-->
<?php get_footer();