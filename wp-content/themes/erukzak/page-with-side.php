<?php /*Template Name: С сайдбаром*/ ?>
<?php get_header() ?>
  <div class="container">

    <ul class="breadcrumb" typeof="BreadcrumbList" vocab="https://schema.org/">
		<?php if ( function_exists( 'bcn_display_list' ) ) {
			bcn_display_list();
		} ?>
    </ul>

    <div class="row">
      <aside id="column-left" class="col-sm-3 hidden-xs">
        <div class="widget">
          <div class="widget-heading">Информация</div>
          <div class="widget-content">
			  <?php
			  wp_nav_menu( array(
				  'theme_location'  => 'side',
				  'menu'            => '',
				  'container'       => '',
				  'container_class' => '',
				  'container_id'    => '',
				  'menu_class'      => 'nav-info',
				  'menu_id'         => '',
				  'echo'            => true,
				  'fallback_cb'     => 'wp_page_menu',
				  'before'          => '',
				  'after'           => '',
				  'link_before'     => '',
				  'link_after'      => '',
				  'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				  'depth'           => 0,
				  'walker'          => '',
			  ) );
			  ?>
          </div>
        </div>
      </aside>
      <div id="content" class="col-sm-9">
		  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			  <?php the_title( '<h1>', '</h1>' ) ?>
			  <?php the_content() ?>
		  <?php endwhile; ?>
		  <?php else: ?>
		  <?php endif; ?>
      </div>
    </div>
  </div>
<?php get_footer();