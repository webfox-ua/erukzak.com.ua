<?php /*Template Name: Отзывы*/ ?>
<?php get_header() ?>
    <div class="container">

        <ul class="breadcrumb" typeof="BreadcrumbList" vocab="https://schema.org/">
			<?php if ( function_exists( 'bcn_display_list' ) ) {
				bcn_display_list();
			} ?>
        </ul>

        <div class="row">
            <div id="content">

                <h1><?php the_title() ?></h1>
                <div class="buttons text-center"><a class="btn  btn-lg btn-primary" data-toggle="modal"
                                                    href="#modal-review"
                                                    title="Написать отзыв"><?php esc_html_e('Оставить отзыв','erukzak'); ?></a></div>
                <div class="review-list">
                    <div class="testimonial-row">
						<?php $query = new WP_Query( array( 'post_type' => 'reviews' ) ) ?>
						<?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
                            <div class="testimonial-col">
                                <div class="review__item">
                                    <div class="review__item-head">
                                        <div class="author"><?php the_title() ?></div>
                                        <div class="date"><?php the_time( 'd.m.Y' ) ?></div>
                                    </div>
                                    <div class="review__item-body">
										<?php the_content() ?>
                                    </div>

                                </div>
                            </div>
						<?php endwhile; ?>
						<?php else: ?>
						<?php endif; ?>
                    </div>

                </div>
                <div class="clearfix"></div>
                <div class="taxonomy__pagination">

                    <button class="btn" data-toggle="modal" data-target="#modal-review"><?php esc_html_e('Оставить отзыв','erukzak'); ?></button>

					<?php
					$args = array(
						'prev_next' => true,
						'prev_text' => __( '&laquo;' ),
						'next_text' => __( '&raquo;' ),
						'type'      => 'list',
						'total'     => $query->max_num_pages,

					);
					echo str_replace( 'page-numbers', 'pagination', paginate_links( $args ) );
					?>
                </div>

            </div>
        </div>
    </div>
<?php get_footer();