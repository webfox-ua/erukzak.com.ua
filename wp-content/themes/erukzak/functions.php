<?php
// Composer autoload
use FS\FS_Config;

require_once "vendor/autoload.php";
// Carbon fields
require_once "inc/carbon-fields.php";
add_action( 'after_setup_theme', 'crb_load' );
function crb_load() {
	\Carbon_Fields\Carbon_Fields::boot();
}


// данная функция включает дополнительные опции вашей темы
function erukzak_theme_setup() {
	// включаем поддержку языков
	load_theme_textdomain( 'erukzak', get_template_directory() . '/languages' );
	// включаем авто добавление тега title в секцию HEAD
	add_theme_support( 'title-tag' );
	// включаем поддержку миниатюр для темы
	add_theme_support( 'post-thumbnails' );
	// set_post_thumbnail_size( 825, 510, true );
	// регистрируем меню
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'erukzak' ),
		'bottom'  => __( 'Bottom Menu', 'erukzak' ),
		'side'    => __( 'Боковое меню', 'erukzak' )
	) );
	// включаем HTML5 разметку для некоторых видов контента
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption'
	) );
	// включаем поддержку собственного логотипа
	add_theme_support( 'custom-logo', array(
		'height'      => 100,
		'width'       => 100,
		'flex-height' => true,
	) );
	// подключаем стили в визуальный редактор
	add_editor_style( array( 'style.css' ) );

	register_sidebar( array(
		'name'          => __( 'Сайдбар в категории товара' ),
		'id'            => "category_sidebar",
		'description'   => '',
		'class'         => 'bf-attr-block',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => "</div></div>\n",
		'before_title'  => '<div class="widgettitle"><i class="icon icon-arrow-top"></i>',
		'after_title'   => "</div><div class=\"widget-content\">\n",
	) );
	register_sidebar( array(
		'name'          => __( 'Подвал (блок слева)' ),
		'id'            => "footer_left",
		'description'   => '',
		'class'         => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => "</div>\n",
		'before_title'  => '<div class="widgettitle">',
		'after_title'   => "</div>\n",
	) );
	register_sidebar( array(
		'name'          => __( 'Подвал (центральный блок)' ),
		'id'            => "footer_center",
		'description'   => '',
		'class'         => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => "</div>\n",
		'before_title'  => '<div class="widgettitle">',
		'after_title'   => "</div>\n",
	) );
	register_sidebar( array(
		'name'          => __( 'Подвал (блок справа)' ),
		'id'            => "footer_right",
		'description'   => '',
		'class'         => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => "</div>\n",
		'before_title'  => '<div class="widgettitle">',
		'after_title'   => "</div>\n",
	) );

}

add_action( 'after_setup_theme', 'erukzak_theme_setup' );

/**
 * подключаем скрипты и стили в шаблон
 *
 */
function erukzak_scripts_and_styles() {
	wp_enqueue_style( 'erukzak-bootstrap', get_template_directory_uri() . '/modules/bootstrap-4.4.1-dist/css/bootstrap.min.css' );
	wp_enqueue_style( 'erukzak-futurademicc-font', get_template_directory_uri() . '/fonts/FuturaDemiC/stylesheet.css' );
	wp_enqueue_style( 'erukzak-futuralightc-font', get_template_directory_uri() . '/fonts/FuturaLightC/stylesheet.css' );
	wp_enqueue_style( 'erukzak-helveticaneuecyr-font', get_template_directory_uri() . '/fonts/HelveticaNeueCyr/stylesheet.css' );
	wp_enqueue_style( 'erukzak-futurafuturis-lightc-font', get_template_directory_uri() . '/fonts/FuturaFuturisLightC/stylesheet.css' );
	wp_enqueue_style( 'erukzak-sprite', get_template_directory_uri() . '/sprite.css' );
	wp_enqueue_style( 'erukzak-slick', get_template_directory_uri() . '/js/slick/slick.css' );
//	wp_enqueue_style( 'erukzak-bootstrap-select', get_template_directory_uri() . '/js/bootstrap-select-1.12.4/dist/css/bootstrap-select.min.css' );
	wp_enqueue_style( 'erukzak-jquery-ui', get_template_directory_uri() . '/css/jquery-ui.min.css' );
	wp_enqueue_style( 'erukzak-formstyler', get_template_directory_uri() . '/css/jquery.formstyler.min.css' );
	wp_enqueue_style( 'erukzak-star-rating', get_template_directory_uri() . '/css/star-rating.min.css' );
	wp_enqueue_style( 'erukzak-number', get_template_directory_uri() . '/css/number.min.css' );
	wp_enqueue_style( 'erukzak-cluetip', get_template_directory_uri() . '/css/jquery.cluetip.css' );
	wp_enqueue_style( 'erukzak-nitro', get_template_directory_uri() . '/css/1-nitro-combined-93e5edf7fbd05e2f27dc36cb6e43a526.css' );
	wp_enqueue_style( 'erukzak-category', get_template_directory_uri() . '/css/articles.css' );
	wp_enqueue_style( 'erukzak-taxonomy', get_template_directory_uri() . '/css/taxonomy.css' );
	wp_enqueue_style( 'erukzak-main', get_template_directory_uri() . '/css/stylesheet.css' );
	wp_enqueue_style( 'erukzak-style-media', get_template_directory_uri() . '/css/media.css' );
	wp_enqueue_style( 'erukzak-style', get_stylesheet_uri() );

	wp_enqueue_script( 'erukzak-facebook-jssdk', get_template_directory_uri() . '/js/all.js', array( 'jquery' ), false, true );
	wp_enqueue_script( 'erukzak-watch', get_template_directory_uri() . '/js/watch.js', array( 'jquery' ), false, true );
//	wp_enqueue_script( 'erukzak-bootstrap-select', get_template_directory_uri() . '/js/bootstrap-select-1.12.4/dist/js/bootstrap-select.min.js', array( 'jquery' ), false, true );
	wp_enqueue_script( 'erukzak-slick', get_template_directory_uri() . '/js/slick/slick.min.js', array( 'jquery' ), false, true );
	wp_enqueue_script( 'erukzak-jquery-ui', get_template_directory_uri() . '/js/jquery-ui.min.js', array( 'jquery' ), false, true );
	wp_enqueue_script( 'erukzak-popperjs', 'https://unpkg.com/@popperjs/core@2', array( 'jquery' ), 'false', true );
	wp_enqueue_script( 'erukzak-bootstrap', get_template_directory_uri() . '/modules/bootstrap-4.4.1-dist/js/bootstrap.min.js', array( 'jquery','erukzak-popperjs' ), 'false', true );
	wp_enqueue_script( 'erukzak-formstyler', get_template_directory_uri() . '/js/jquery.formstyler.min.js', array( 'jquery' ), false, true );
	wp_enqueue_script( 'erukzak-star-rating', get_template_directory_uri() . '/js/star-rating.min.js', array( 'jquery' ), false, true );
	wp_enqueue_script( 'erukzak-core', get_template_directory_uri() . '/js/core.min.js', array( 'jquery' ), false, true );
	wp_enqueue_script( 'erukzak-number', get_template_directory_uri() . '/js/number.min.js', array( 'jquery' ), false, true );
	wp_enqueue_script( 'erukzak-maskedinput', get_template_directory_uri() . '/js/jquery.maskedinput.min.js', array( 'jquery' ), false, true );
	wp_enqueue_script( 'erukzak-nicescroll', get_template_directory_uri() . '/js/jquery.nicescroll/dist/jquery.nicescroll.min.js', array( 'jquery' ), false, true );
	wp_enqueue_script( 'erukzak-elevateZoom', get_template_directory_uri() . '/js/jquery.elevateZoom-2.2.3.min.js', array( 'jquery' ), false, true );
	wp_enqueue_script( 'erukzak-jcarousel', get_template_directory_uri() . '/js/jquery.jcarousel.min.js', array( 'jquery' ), false, true );
	wp_enqueue_script( 'erukzak-common', get_template_directory_uri() . '/js/common.js', array( 'jquery' ), false, true );
//	wp_enqueue_script( 'erukzak-cluetip', get_template_directory_uri() . '/js/jquery.cluetip.js', array( 'jquery' ), false, true );
	wp_enqueue_script( 'erukzak-owl-carousel', get_template_directory_uri() . '/js/owl.carousel.min.js', array( 'jquery' ), false, true );
	wp_enqueue_script( 'erukzak-maskedinput', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js', array( 'jquery' ), false, true );
	wp_enqueue_script( 'erukzak-magnific-popup', get_template_directory_uri() . '/js/jquery.magnific-popup.min.js', array( 'jquery' ), false, true );

	wp_enqueue_script( 'erukzak-main', get_template_directory_uri() . '/js/main.js', array( 'jquery' ), false, true );
}

add_action( 'wp_enqueue_scripts', 'erukzak_scripts_and_styles' );

function admin_enqueue_scripts__action() {
	wp_enqueue_script( 'erukzak-admin', get_template_directory_uri() . '/js/admin.js', array( 'jquery' ), false, true );
}

add_action( 'admin_enqueue_scripts', 'admin_enqueue_scripts__action' );

/**
 * Функция помогает вывести определённое к-во символов из основного текста поста
 * будет полезна для автоматического получения анонса и более точной обрезки чем стандартная функция the_excerpt(). Не требует дополнительных действий, написания хуков как the_excerpt()
 *
 * @param integer $args - к-во символов текста (!не слов)
 * @param string $end - окончание (по умолчанию многоточие) если размер симолов больше заданного в параметре $args
 *
 * @return выводит правильно обрезанный текст записи
 */
function text_substr( $args = 350, $end = '&hellip;' ) {
	global $post;
	$text = strip_tags( $post->post_excerpt );
	if ( empty( $text ) ) {
		$text = strip_tags( $post->post_content );
	}
	if ( strlen( $text ) <= $args ) {
		$short_text = $text;
	} else {
		$text       = mb_substr( $text, 0, $args );
		$short_text = substr( $text, 0, strrpos( $text, ' ' ) );
		$short_text = $short_text . $end;
	}
	echo $short_text;
}

/**
 * Разделяет фразу из нескольких слов на две части
 * первая часть состоит из первого слова
 * остальная из последующих
 *
 * @param $title фраза которую нужно разделить
 * @param string $first_wrapper обёртка первой части
 * @param string $second_wrapper обёртка для второй части
 *
 * @return string
 */
function kv_title_chunk( $title, $first_wrapper = '%s', $second_wrapper = '<span>%s</span>' ) {
	$chunk = explode( ' ', $title );
	if ( count( $chunk ) >= 2 ) {
		$title = sprintf( $first_wrapper, $chunk[0] );
		unset( $chunk[0] );
		$title .= sprintf( ' ' . $second_wrapper, implode( ' ', $chunk ) );
	}

	return $title;
}

add_filter( 'next_post_link', 'next_post_link_attributes' );
add_filter( 'previous_post_link', 'prev_post_link_attributes' );

function next_post_link_attributes( $output ) {
	$injection = 'class="button-next"';

	return str_replace( '<a href=', '<a ' . $injection . ' href=', $output );
}

function prev_post_link_attributes( $output ) {
	$injection = 'class="button-prev"';

	return str_replace( '<a href=', '<a ' . $injection . ' href=', $output );
}

add_filter( 'excerpt_length', function () {
	return 20;
} );

add_filter( 'excerpt_more', function ( $more ) {
	return '...';
} );

// Делаем неактивным пункт меню в котором находимся
add_filter( 'nav_menu_link_attributes', function ( $atts, $item, $args, $depth ) {

	if ( $item->current == true ) { // for example

		unset( $atts['href'] );
	}

	return $atts;
}, 10, 4 );

// Меняем заголовки в карточке товара
add_filter( 'wpseo_title', 'wpseo_title_filter', 10, 1 );
function wpseo_title_filter( $title ) {
	global $post;

	if ( is_singular( 'product' ) && $post->comment_count ) {
		return sprintf( '%s по цене %s грн. Читать отзывы: %d', get_the_title(), fs_get_price(), intval( $post->comment_count ) );
	} elseif ( is_singular( 'product' ) && ! fs_aviable_product() ) {
		return sprintf( '%s в интернет-магазине Erukzak.com.ua', get_the_title() );
	} elseif ( is_singular( 'product' ) && ! $post->comment_count ) {
		return sprintf( '%s купить по цене %s грн | erukzak.com.ua', get_the_title(), fs_get_price() );
	} elseif ( is_tax( 'catalog' ) && get_query_var( 'paged' ) && get_locale() == 'ru_RU' ) {
		return sprintf( '%s - Страница №%d | erukzak.com.ua ', single_term_title( null, false ), get_query_var( 'paged' ) );
	} elseif ( is_tax( 'catalog' ) && get_query_var( 'paged' ) && get_locale() == 'uk' ) {
		return sprintf( '%s - Сторінка №%d | erukzak.com.ua ', single_term_title( null, false ), get_query_var( 'paged' ) );
	} else {
		return $title;
	}

}

// Меняем описание в карточке товара
add_filter( 'wpseo_metadesc', 'wpseo_metadesc_filter', 10, 1 );
function wpseo_metadesc_filter( $title ) {
	if ( is_singular( 'product' ) ) {
		global $post;

		if ( $post->comment_count ) {
			return sprintf( '%s ✔ цена %s грн ✔ Интернет-супермаркет рюкзаков Erukzak.com.ua', get_the_title(), fs_get_price() );
		} elseif ( ! fs_aviable_product() ) {
			return sprintf( 'Интернет-супермаркет рюкзаков Erukzak.com.ua ➜ %s ✔ артикул %s ✆ (044) 232-66-32', get_the_title(), fs_get_product_code() );

		} else {
			return sprintf( 'Erukzak.com.ua ➜ интернет-супермаркет рюкзаков №❶ %s ✔ цена %s грн ✔ артикул %s ✔ доставка по Киеву и Украине ✆ (044) 232-66-32', get_the_title(), fs_get_price(), fs_get_product_code() );
		}
	}

	return $title;
}

add_filter( 'fs_meta_description', function ( $meta_description ) {
	if ( is_tax( 'catalog' ) && get_query_var( 'paged' ) && get_locale() == 'ru_RU' ) {
		$city = get_locale() == 'ru_RU' ? get_field( 'city_name', 'term_' . get_queried_object_id() ) : get_field( 'city_name_' . get_locale(), 'term_' . get_queried_object_id() );
		if ( $city ) {
			return sprintf( "Интернет-магазин рюкзаков erukzak.com.ua  ✔ Большой выбор в %s ✌ Стильные и удобные модели ➤ Ждём Ваших звонков! ☎ (096) 831-23-61 | Страница №%d", $city, abs( get_query_var( 'paged' ) ) );
		}

		return sprintf( 'Интернет-магазин рюкзаков erukzak.com.ua ✔ Стильные модели на любой вкус ✌ Выгодные и доступные цены ➤ Ждём Ваших заказов! ☎ (096) 831-23-61 | Страница №%d', abs( get_query_var( 'paged' ) ) );
	} elseif ( is_tax( 'catalog' ) && get_query_var( 'paged' ) && get_locale() == 'uk' ) {
		$city = get_locale() == 'ru_RU' ? get_field( 'city_name', 'term_' . get_queried_object_id() ) : get_field( 'city_name_' . get_locale(), 'term_' . get_queried_object_id() );

		if ( $city ) {
			return sprintf( "Інтернет-магазин рюкзаків erukzak.com.ua ✔ Великий вибір в %s ✌ Стильні та зручні моделі ➤ Чекаємо Ваших дзвінків! ☎ (096) 831-23-61 | Cторінка №%d", $city, abs( get_query_var( 'paged' ) ) );
		}

		return sprintf( 'Інтернет-магазин рюкзаків erukzak.com.ua ✔ Стильні моделі на будь-який смак ✌ Вигідні та доступні ціни ➤ Чекаємо Ваших замовлень! ☎ (096) 831-23-61 | Сторінка №%d', abs( get_query_var( 'paged' ) ) );
	} else {
		return $meta_description;
	}
} );

// define the wpseo_canonical callback
function filter_wpseo_canonical( $canonical ) {
	if ( is_tax( 'catalog' ) && get_query_var( 'paged' ) ) {
		$canonical = get_term_link( get_queried_object_id() );
	}

	return $canonical;
}

// add the filter
add_filter( 'wpseo_canonical', 'filter_wpseo_canonical', 10, 1 );

// добавдяем мета кейвордс
add_action( 'wp_head', 'wp_head_action', 1 );
function wp_head_action() {
	if ( ! is_singular( 'product' ) ) {
		return;
	}
	global $post;

	if ( $post->comment_count ) {
		printf( '<meta name="keywords" content="%s, цена, купить, отзывы"/>', get_the_title() );
	} elseif ( ! fs_aviable_product() ) {
		printf( '<meta name="keywords" content="%s, %s"/>', get_the_title(), fs_get_product_code() );
	} else {
		printf( '<meta name="keywords" content="%s, цена, купить"/>', get_the_title() );
	}
}

add_filter( 'template_include', 'erukzak_catalog_template' );

function erukzak_catalog_template( $template ) {
	if ( is_tax( 'catalog' ) ) {
		$template_type = get_field( 'template', 'term_' . get_queried_object_id() );

		if ( $template_type && $template_type == 'template-2' ) {
			$template = locate_template( array( 'archive.php' ) );
		}
	}

	return $template;
}

// На странице категории города отображаем товары родительской категории
add_action( 'pre_get_posts', 'action_function_name_11', 10, 1 );
function action_function_name_11( $query ) {

	if ( is_admin() || ! $query->is_main_query() ) {
		return;
	}

	if ( $query->is_tax ) {
		$term = get_term_by( 'slug', $query->query['catalog'], 'catalog' );


		if ( get_field( 'city_name', 'term_' . $term->term_id ) ) {

			$query->set( 'tax_query', array(
				array(
					'taxonomy' => 'catalog',
					'field'    => 'term_id',
					'terms'    => intval( $term->parent ),
					'operator' => 'IN'

				)
			) );

			$query->set( 'catalog', get_term_field( 'slug', $term->parent ) );
			$query->set( 'paged', get_query_var( 'paged' ) );
			$query->set( 'post_type', 'product' );
			$query->query['catalog']   = get_term_field( 'slug', $term->parent );
			$query->tax_query->queries = [];
		}

		if ( $term->parent != 0 && get_field( 'city_name', 'term_' . $term->parent ) ) {
			$parent_term = get_term_by( 'id', $term->parent, 'catalog' );
			$query->set( 'tax_query', array(
				array(
					'taxonomy' => 'catalog',
					'field'    => 'term_id',
					'terms'    => intval( $parent_term->parent ),
					'operator' => 'IN'

				)
			) );

			$query->set( 'catalog', get_term_field( 'slug', $parent_term->parent ) );
			$query->set( 'paged', get_query_var( 'paged' ) );
			$query->set( 'post_type', 'product' );
			$query->query['catalog']   = get_term_field( 'slug', $parent_term->parent );
			$query->tax_query->queries = [];
		}


	} elseif ( get_query_var( 'terms' ) ) {
		$terms     = get_query_var( 'terms' );
		$tax_query = [];
		if ( is_array( $terms ) && count( $terms ) ) {
			foreach ( $terms as $tax => $term ) {
				$tax_query[] = array(
					'taxonomy' => $tax,
					'field'    => 'slug',
					'terms'    => $term
				);
			}
		}

		$query->set( 'tax_query', $tax_query );
		$query->set( 'post_type', 'product' );
	}

	if ( $query->is_tax && get_field( 'tax_display', 'term_' . $term->term_id ) ) {
		$tax_display = get_field( 'tax_display', 'term_' . $term->term_id );

		$query->set( 'catalog', false );
		$query->set( 'post_type', 'product' );
		$query->set( 'tax_query', [
			[
				'taxonomy' => 'product-attributes',
				'field'    => 'term_id',
				'terms'    => $tax_display,
				'operator' => 'IN'
			]
		] );
	}
}


function custom_taxonomy_title() {
	if ( get_query_var( 'terms' ) ) {
		$terms_names = [];
		$increment   = 0;
		foreach ( get_query_var( 'terms' ) as $tax => $item ) {
			$term_name = get_term_by( 'slug', $item, $tax );
			if ( is_wp_error( $term_name ) || ! isset( $term_name->name ) ) {
				continue;
			}
			$name = $term_name->name;
			$name = str_replace( [ 'рюкзаки' ], '', $name );
			if ( $increment > 0 ) {
				$name = mb_strtolower( $name );
			}
			$terms_names[] = $name;
			$increment ++;
		}
		$terms_names[] = 'рюкзаки';
		echo implode( ' ', $terms_names );
	} else {
		if ( get_query_var( 'paged' ) && get_locale() == 'ru_RU' ) {
			printf( '%s – Страница №%d', single_term_title( null, false ), abs( get_query_var( 'paged' ) ) );
		} elseif ( get_query_var( 'paged' ) && get_locale() == 'uk' ) {
			printf( '%s – Сторінка №%d', single_term_title( null, false ), abs( get_query_var( 'paged' ) ) );
		} else {
			single_term_title();
		}

	}
}


function my_acf_flexible_content_layout_title( $title, $field, $layout, $i ) {

	// load text sub field
	if ( $text = get_sub_field( 'title' ) ) {

		$title .= ': ' . $text;

	}

	// return
	return $title;

}

// name
add_filter( 'acf/fields/flexible_content/layout_title', 'my_acf_flexible_content_layout_title', 10, 4 );
function my_acf_admin_head() {
	?>
    <script type="text/javascript">
        (function ($) {

            $(document).ready(function () {

                $(".-collapse").each(function (index) {
                    $(this).click();
                });

            });

        })(jQuery);
    </script>
	<?php
}

add_action( 'acf/input/admin_head', 'my_acf_admin_head' );

add_action( 'admin_init', function () {
	if ( is_admin() && isset( $_GET['acf'] ) && $_GET['acf'] == 'populate' && isset( $_GET['tag_ID'] ) ) {
		$field = get_field( 'selection_backpacks', 1933 );
		update_field( 'selection_backpacks', $field, 'term_' . $_GET['tag_ID'] );
		wp_safe_redirect( remove_query_arg( 'acf' ) );
	}

	return;
} );


// Исключаем страницы из карты сайта
add_filter( 'wpseo_exclude_from_sitemap_by_post_ids', function () {

	global $wpdb;
	$items = $wpdb->get_results( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key='exclude_from_sitemap' AND meta_value='1'", ARRAY_N );
	$items = array_shift( $items );
	if ( $items ) {
		return $items;
	}

	return [];
} );

add_filter( 'wpseo_sitemap_exclude_empty_terms', '__return_false' );

// Дополнительные поля категории товара
add_filter( 'fs_taxonomy_fields', function ( $fields ) {
	$fields[ FS_Config::get_data( 'product_taxonomy' ) ]['_alternative_name'] = [
		'name' => __( 'Альтернативное название', 'f-shop' ),
		'type' => 'text',
		'args' => array()
	];

	return $fields;
} );
