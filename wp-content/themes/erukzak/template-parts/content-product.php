<?php
/**
 * Created by PhpStorm.
 * User: karak
 * Date: 27.07.2018
 * Time: 20:58
 */ ?>
<div class="product-thumb">
    <div class="animate-h">
	    <?php if ( fs_is_label( 'pop' ) ): ?>
            <div class="text-special text-special-pop"><?php esc_html_e( 'популярное', 'erukzak' ); ?></div>
	    <?php elseif ( fs_is_label( 'new' ) ): ?>
            <div class="text-special text-special-new"><?php esc_html_e( 'новинка', 'erukzak' ); ?></div>
	    <?php elseif ( fs_is_label( 'promo' ) || fs_is_action() ): ?>
            <div class="text-special"><?php esc_html_e( 'акция', 'erukzak' ); ?></div>
	    <?php endif; ?>
        <div class="image">


			<?php $gallery = fs_get_gallery( 0, true, true ) ?>
            <div class="product-gallery">
				<?php foreach ( $gallery as $gal ): ?>
                    <div>
                        <a href="<?php the_permalink() ?>">
                            <picture>
                                <img src="<?php echo wp_get_attachment_image_url( $gal ); ?>"
                                     alt="<?php echo get_the_title() ?>">
                            </picture>
                        </a>
                    </div>
				<?php endforeach; ?>
            </div>

        </div>
        <div class="hover">
            <div class="caption">
                <div class="h4"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></div>
                <p class="price">
					<?php fs_the_price() ?>
					<?php fs_base_price() ?>
                </p>
            </div>
            <div class="button-group">
				<?php fs_add_to_cart( 0, '<span class="icon icon-cart"></span> ' . esc_html__( 'Купить', 'erukzak' ), array( 'class' => 'btn btn-primary btn-block' ) ) ?>
            </div>
        </div>
    </div>
</div>
