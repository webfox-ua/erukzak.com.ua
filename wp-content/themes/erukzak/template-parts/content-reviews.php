<?php
/**
 * Created by PhpStorm.
 * User: karak
 * Date: 01.08.2018
 * Time: 18:23
 */ ?>
<div class="review-item">
	<div class="title"><?php the_title() ?></div>
	<?php the_content() ?>
  <div class="date-time"><span><?php the_time('d.m.Y') ?></span></div>
</div>
