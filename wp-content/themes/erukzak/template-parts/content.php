<?php
/**
 * Created by PhpStorm.
 * User: karak
 * Date: 30.07.2018
 * Time: 19:20
 */ ?>
<div class="col-sm-6">
  <div class="articleCat" itemscope="" itemtype="http://schema.org/Article">
    <div class="row">
      <div class="col-sm-6">
        <div class="article-image">
          <a class="imageFeaturedLink" href="<?php the_permalink() ?>" title="<?php the_title() ?>" itemprop="url">
			  <?php if ( has_post_thumbnail() )
				  the_post_thumbnail( 'full', array( 'class' => 'imageFeatured img-responsive center-block' ) ) ?>
          </a>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="articleHeader">
          <h3 itemprop="name"><a href="<?php the_permalink() ?>" title="<?php the_title() ?>"
                                 itemprop="url"><?php the_title() ?></a></h3>
          <span>от <span itemprop="dateCreated"><?php the_time( 'F d, Y' ) ?></span>.</span>
        </div>
        <div class="articleContent">
					<span class="blockClear" itemprop="description">
						<?php the_excerpt() ?>
					</span>
          <div class="readMore text-center">
            <a class="btn btn-default-white" href="<?php the_permalink() ?>" title="<?php the_title() ?>">Подробнее</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
