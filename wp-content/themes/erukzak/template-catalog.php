<?php
/*Template name: Products*/
get_header() ?>
    <div class="container">
        <ul class="breadcrumb" typeof="BreadcrumbList" vocab="https://schema.org/">
			<?php if ( function_exists( 'bcn_display_list' ) ) {
				bcn_display_list();
			} ?>
        </ul>

        <div class="row">
            <div id="content" class="col-sm-12">
                <h1><?php the_title() ?></h1>
                <div class="row product-list">
					<?php
					$meta_key = apply_filters( 'the_title', get_post_meta( get_the_ID(), 'show_only', 1 ) );
					query_posts( [
						'post_type'  => 'product',
						'meta_key'   => $meta_key,
						'meta_value' => 1,
						'paged'      => get_query_var( 'paged' )
					] );
					if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                        <div class="product-layout col-sm-6 col-md-3">
							<?php get_template_part( 'template-parts/content', get_post_type() ) ?>
                        </div>
					<?php endwhile; ?>
					<?php else: ?>
                        <p class="col-md-12 alert alert-warning" style="margin-left: 15px;">Не найдено товаров по вашему
                            запросу</p>
					<?php endif; ?>
                </div>
                <div class="taxonomy__pagination">

					<?php if ( $wp_query->max_num_pages > 1 ): ?>
                        <button class="btn">показать еще</button>
					<?php endif ?>
					<?php
					$args = array(
						'prev_next' => true,
						'prev_text' => __( '&laquo;' ),
						'next_text' => __( '&raquo;' ),
						'type'      => 'list',

					);
					echo str_replace( 'page-numbers', 'pagination', paginate_links( $args ) );
					wp_reset_query();
					?>
                </div>

            </div>
        </div><!--row-->
    </div><!--container-->
<?php if ( get_the_content() ) : ?>
    <section class="section section-dark page__content">
        <div class="container">
            <div class="page-text">
                <div class="nice-scroll">
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<?php the_content() ?>
					<?php endwhile; ?>
					<?php else: ?>
					<?php endif; ?>
                </div>
            </div>

        </div>
    </section>
<?php endif; ?>
<?php get_footer();