<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitcb00a39dca7bc26a7a97c2dae9f4e332
{
    public static $prefixLengthsPsr4 = array (
        'C' => 
        array (
            'Carbon_Fields\\' => 14,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Carbon_Fields\\' => 
        array (
            0 => __DIR__ . '/..' . '/htmlburger/carbon-fields/core',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitcb00a39dca7bc26a7a97c2dae9f4e332::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitcb00a39dca7bc26a7a97c2dae9f4e332::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
