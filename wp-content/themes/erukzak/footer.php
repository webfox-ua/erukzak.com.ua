<footer class="section section-dark site__footer ">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
				<?php if ( function_exists( 'dynamic_sidebar' ) ) {
					dynamic_sidebar( 'footer_left' );
				} ?>
            </div>
            <div class="col-lg-3">
				<?php if ( function_exists( 'dynamic_sidebar' ) ) {
					dynamic_sidebar( 'footer_center' );
				} ?>
            </div>
            <div class="col-lg-4">
				<?php if ( function_exists( 'dynamic_sidebar' ) ) {
					dynamic_sidebar( 'footer_right' );
				} ?>
            </div>
        </div>
    </div>
</footer>
<div class="bottom__line">
    <div class="container">
        <div class="copy"><?php echo apply_filters( 'the_title', carbon_get_theme_option( 'crb_copywrite' ) ); ?></div>
        <div class="social">
            <a href="https://www.facebook.com/erukzakcomua/" class="icon icon-fb" rel="nofollow"></a>
            <a href="https://www.instagram.com/erukzakua/" class="icon icon-insta" rel="nofollow"></a>
            <a href="https://twitter.com/erukzak" class="icon icon-tw" rel="nofollow"></a>
        </div>
        <div class="develop">
			<?php echo apply_filters( 'the_title', carbon_get_theme_option( 'crb_developer_link' ) ); ?>
        </div>
    </div>
</div>
<div class="totop" id="toTop">
    <img src="/wp-content/themes/erukzak/img/totop.png" alt="Наверх">
</div>
<?php wp_footer() ?>
<?php echo carbon_get_theme_option( 'crb_footer_script' ) ?>
<div class="modal fade" id="modal-review">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="modal-title h4"><i
                            class="icon icon-write"></i> <?php esc_html_e( 'Написать отзыв', 'erukzak' ); ?></div>
            </div>
            <div class="modal-body">
				<?php
				if ( get_locale() == 'uk' ) {
					echo do_shortcode( '[contact-form-7 id="5590" title="Оставить отзыв_ua"]' );
				} else {
					echo do_shortcode( '[contact-form-7 id="5566" title="Оставить отзыв"]' );
				}
				?>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modal-back-call">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="modal-title h4"><i
                            class="icon icon-write"></i> <?php esc_html_e( 'Обратный звонок', 'erukzak' ); ?></div>
            </div>
            <div class="modal-body">
				<?php
				if ( get_locale() == 'uk' ) {
					echo do_shortcode( '[contact-form-7 id="5586" title="Обратный звонок"]' );
				} else {
					echo do_shortcode( '[contact-form-7 id="5585" title="Обратный звонок"]' );
				}
				?>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modal-cart">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="modal-title h4">
                    <img src="<?php echo get_template_directory_uri() ?>/img/basket.png"
                         alt="Cart">
					<?php esc_html_e( 'Корзина', 'erukzak' ); ?>
                </div>
            </div>
            <div class="modal-body" data-fs-element="cart-widget" data-template="cart-widget/modal-cart">
				<?php get_template_part( 'f-shop/cart-widget/modal-cart' ) ?>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


</body>
</html>