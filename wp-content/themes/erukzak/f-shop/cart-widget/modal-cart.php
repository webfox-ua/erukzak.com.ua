<?php
$cart_items = fs_get_cart();
if ( $cart_items ) {
	echo '<ul class="modal-cart__items">';
	foreach ( $cart_items as $key => $cart_item ) {
		$product = fs_set_product( $cart_item, $key );
		echo '<li class="modal-cart__item">';
		echo '<div class="modal-cart__item-thumb">' . $cart_item['thumbnail'] . '</div>';
		echo '<div class="modal-cart__item-meta">';
		echo '<div class="modal-cart__item-name">' . $cart_item['name'] . '</div>';
		echo '<div class="modal-cart__item-prices">';
		echo '<div class="modal-cart__item-price">';
		$product->the_price();
		echo '</div>';
		echo '<div class="quantity-block">';
		$product->cart_quantity( array(
			'refresh' => false,
			'pluss'   => array(
				'class'   => 'fs-number-arrow fs-number-up',
				'content' => ''
			),
			'minus'   => array(
				'class'   => 'fs-number-arrow fs-number-down',
				'content' => ''
			),
		) );
		echo '</div>';
		echo '<div class="modal-cart__item-price">';
		$product->the_cost();
		echo '</div>';
		echo '</div>';
		echo '</div>';
		fs_delete_position( $key, [ 'content' => '&times;', 'refresh' => false ] );
		echo '</li>';
	}
	echo '</ul>';
	?>
    <div class="modal-cart__total">
        <span class="modal-cart__total-text"><?php esc_html_e( 'Итого:' ) ?></span> <?php fs_total_amount() ?>
    </div>
    <div class="btn-groups d-flex justify-content-between">
        <button class="btn btn-outline-warning"
                data-dismiss="modal"><?php esc_html_e( 'продолжить покупки', 'erukzak' ) ?></button>
        <a href="<?php fs_checkout_url() ?>"
           class="btn btn-outline-primary"><?php esc_html_e( 'оформить заказ', 'erukzak' ) ?></a>
    </div>
	<?php
} else { ?>
    <p style="margin-top: 30px;"><?php esc_html_e( 'Ваша корзина пуста', 'erukzak' ) ?></p>
<?php }
?>