<div class="sprite cart-count">
  <span id="cart-total"><?php fs_product_count() ?></span>
</div>
<a href="<?php fs_cart_url() ?>" class="btn btn-link"><?php esc_html_e('Корзина','erukzak'); ?></a>
<div class="hidden">
  <div id="cart-content" class="modal-content mfp-with-anim">
    <div class="widget-heading"><span class="sprite cart-heading"></span> <?php esc_html_e('Корзина','erukzak'); ?></div>
    <p class="text-center"><?php esc_html_e('Ничего не куплено!','erukzak'); ?></p>
    <button title="Close (Esc)" type="button" class="mfp-close">×</button> 
  </div>
</div>
