<?php
/**
 * Product Basket Template
 */
?>
<div class="fs-cart-listing table-responsive">
	<?php if ( $cart ): ?>
        <table class="table table-condensed">
            <thead class="thead-light">
            <tr>
                <td>
					<?php esc_html_e( 'Photo', 'f-shop' ); ?>
                </td>
                <td>
					<?php esc_html_e( 'Product', 'f-shop' ); ?>
                </td>
                <td>
					<?php esc_html_e( 'Vendor code', 'f-shop' ); ?>
                </td>
                <td>
					<?php esc_html_e( 'Price', 'f-shop' ); ?>
                </td>
                <td>
					<?php esc_html_e( 'Quantity', 'f-shop' ); ?>
                </td>
                <td>
					<?php esc_html_e( 'Cost', 'f-shop' ); ?>
                </td>
                <td></td>
            </tr>
            </thead>
            <tbody>
			<?php
			foreach ( $cart as $item_id => $product ): ?>
				<?php $item = fs_set_product( $product, $item_id ) ?>
                <tr>
                    <td>
						<?php fs_product_thumbnail( $item->id ) ?>
                    </td>
                    <td>
                        <div class="info">
                            <a href="<?php echo esc_url( $item->permalink ) ?>" target="_blank"
                               class="name"><?php echo esc_html( $item->title ) ?></a>
                        </div>
                    </td>
                    <td>
						<?php $item->the_sku() ?>
                    </td>
                    <td style="white-space: nowrap;" class="price-cell">
						<?php $item->the_price() ?>
                    </td>
                    <td>
                        <div class="quantity-block">
							<?php $item->cart_quantity( array(
								'pluss' => array(
									'class'   => 'fs-number-arrow fs-number-up',
									'content' => ''
								),
								'minus' => array(
									'class'   => 'fs-number-arrow fs-number-down',
									'content' => ''
								),
							) ) ?>
                        </div>

                    </td>
                    <td style="white-space: nowrap;" class="price-cell">
						<?php $item->the_cost() ?>
                    </td>
                    <td>
						<?php $item->delete_position( array(
							'class'   => 'btn btn-primary',
							'type'    => 'button',
							'content' => '<i class="fa fa-times" aria-hidden="true"></i>'
						) ) ?>
                    </td>
                </tr>
			<?php endforeach; ?>
            </tbody>
        </table>
        <div class="fs-products-after">

            <div></div>
            <div class="cart-total-amount"><span><?php esc_html_e('Итого','erukzak'); ?>:</span> <?php fs_total_amount() ?></div>

        </div>

        <div class="fs-products-after">
            <div></div>
            <a href="<?php echo fs_get_catalog_link() ?>"
               class="btn btn-outline-primary"><?php esc_html_e( 'Продолжить покупки', 'erukzak' ); ?></a>

        </div>

	<?php else: ?>
        <p class="fs-info-block"><span
                    class="icon glyphicon glyphicon-info-sign"></span> <?php esc_html_e( 'Your basket is empty', 'f-shop' ) ?>
            .&nbsp;
            <a
                    href="<?php echo esc_url( fs_get_catalog_link() ) ?>"><?php esc_html_e( 'To the catalog', 'f-shop' ) ?></a>
        </p>
	<?php endif; ?>
</div>
