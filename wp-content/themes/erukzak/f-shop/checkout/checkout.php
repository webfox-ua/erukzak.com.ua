<?php echo do_shortcode( '[fs_cart]' ); ?>

<h2 class="text-center" style="margin-bottom: 30px;"><?php esc_html_e( 'Оформить заказ', 'erukzak' ); ?></h2>
<div class="row">
    <div class="col-md-6">

        <div class="form-group">
			<?php fs_form_field( 'fs_first_name' ) ?>
        </div>

        <div class="form-group">
			<?php fs_form_field( 'fs_email' ) ?>
        </div>

        <div class="form-group">
			<?php fs_form_field( 'fs_phone' ) ?>
        </div>
        <div class="form-group">
			<?php fs_form_field( 'fs_comment',['placeholder'=>__('Комментарий к заказу','erukzak'),'type'=>'text'] ) ?>
        </div>

    </div>
    <div class="col-md-6">
        <div class="form-group">
		    <?php fs_form_field( 'fs_delivery_methods' ) ?>
        </div>
        <div class="form-group">
			<?php fs_form_field( 'fs_city' ) ?>
        </div>
        <div class="form-group">
			<?php fs_form_field( 'fs_delivery_number' ) ?>
        </div>

        <div class="form-group">
			<?php fs_form_field( 'fs_payment_methods' ) ?>
        </div>



    </div>
    <div class="col-md-12 d-flex justify-content-center">
		<?php fs_order_send( __( 'Подтвердить заказ', 'erukzak' ), [ 'class' => 'btn btn-primary btn-lg' ] ); ?>
    </div>
</div>

