<?php
/**
 * Price Range Slider
 */ ?>
<div class="slider" data-fs-element="jquery-ui-slider">
    <div data-fs-element="range-slider" id="range-slider"></div>
    <div class="clearfix"></div>
    <div class="price--inputs">
        <input type="text" value="<?php echo esc_attr( $args['price_start'] ) ?>"
               data-fs-element="range-start-input"
               data-url="<?php fs_filter_link( [], null, array( 'price_start' ) ) ?>">
        <input type="text" value="<?php echo esc_attr( $args['price_max'] ) ?>" data-fs-element="range-end-input"
               data-url="<?php fs_filter_link( [], null, array( 'price_end' ) ) ?>">
    </div>
</div>
