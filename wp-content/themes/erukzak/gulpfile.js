var gulp = require('gulp');
var spritesmith = require('gulp.spritesmith');
var replace = require('gulp-replace')
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');

var config = {
    "server": "erukzak.loc", // урл локального сайта
    "sassPath": "css/*.scss", // где отслеживать scss файлы
    "sassRoot": "css/", // путь к папке с scss файлами
    "jsPath": "js/*.js", // путь к папке с js файлами
    "cssPath": ".", // куда положить файл style.css, для Вордпресс должен быть в корне темы
    "iconPath": "img/icons/*.png", // путь к иконкам, для создания спрайта
    "wpTheme": {
        name: "Erukzak", // название вашей темы
        domain: "erukzak", // textdomain
        author: "Vitaliy Karakushan", // имя и фамилия автора
        autorUri: "https://profglobal.pro" // ссылка на сайт разработчика
    }
}

var replaceStrings = {
    "functions.php": {
        0: {search: "%textdomain%", replace: config.wpTheme.domain},
        1: {search: "%prefix%", replace: config.wpTheme.domain.replace('-', '_')}
    },
    "style.scss": {
        0: {search: "%textdomain%", replace: config.wpTheme.domain},
        1: {search: "%theme_name%", replace: config.wpTheme.name},
        2: {search: "%theme_autor_uri%", replace: config.wpTheme.autorUri},
        3: {search: "%theme_autor%", replace: config.wpTheme.author}
    }
}

// Этот таск нужно запустить перед началом проекта
gulp.task('start', function () {

    // переименовываем переменные в файлах
    for (var key in replaceStrings) {
        var src = gulp.src([key]);
        for (var s in replaceStrings[key]) {
            src.pipe(replace(replaceStrings[key][s].search, replaceStrings[key][s].replace))

        }
        src.pipe(gulp.dest('.'));
    }

});


// Автоматическое создание спрайтов
gulp.task('sprite', function () {
    var spriteData = gulp.src(config.iconPath)
        .pipe(spritesmith({
            imgName: 'sprite.png',
            cssName: 'sprite.css',
            cssVarMap: function (sprite) {
                if (sprite.name.indexOf('-hover') !== -1) {
                    sprite.name = 'icon-' + sprite.name.replace('-hover', ':hover');
                } else {
                    sprite.name = 'icon-' + sprite.name;
                }
            },
            imgPath: 'sprite.png',
            cssOpts: {
                cssSelector: function (item) {
                    // If this is a hover sprite, name it as a hover one (e.g. 'home-hover' -> 'home:hover')
                    if (item.name.indexOf('-hover') !== -1) {
                        return '.icon-' + item.name.replace('-hover', ':hover');
                        // Otherwise, use the name as the selector (e.g. 'home' -> 'home')
                    }
                    else {
                        return '.' + item.name;
                    }
                }
            }
        }));
    spriteData.css.pipe(gulp.dest('.'));
    spriteData.img.pipe(gulp.dest('.'));
});

// перезагрузка при изменениях
gulp.task('serve', ['sass'], function () {

    browserSync.init({
        proxy: config.server
    });

    gulp.watch(config.sassPath, ['sass']);
    gulp.watch("*.php").on('change', browserSync.reload);
    gulp.watch("js/*.js").on('change', browserSync.reload);
    gulp.watch(config.iconPath, ['sprite']);
});

gulp.task('sass', function () {
    return gulp.src(config.sassPath)
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(config.cssPath))
        .pipe(browserSync.stream());
});


// этот таск запускается автоматом при вводе команды gulp
gulp.task('default', ['sprite', 'serve']);
