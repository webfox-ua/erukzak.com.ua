<?php get_header() ?>
  <!--EOF Color series-->
  <div class="container">
    <div class="row">
      <div id="content" class="col-sm-12">
        <div class="row">
          <div class="hidden">
            <div id="modal_form0" class="widget modal-content mfp-with-anim mfp-hide">
              <div class="widget-heading h3">
                <i class="sprite call-me"></i> Заказать звонок
              </div>
              <div class="widget-content">
                <div id="webme_sidebar_feedback0" class="middle">
                  <div id="tmp_0"></div>
                  <p class="text-center">Закажите звонок сейчас и мы наберем вас на протяжении 25-ти минут</p>
                  <div id="wsf_enquiry_send_result0"></div>
                  <div id="sidebar_feedback_div0">
                    <form action="<?php echo home_url(); ?>/index.php?route=module/feedback/feedback" method="post"
                          enctype="multipart/form-data" id="sidebar_feedback0">
                      <input type="hidden" id="form_id0" name="form_id" value="0">
                      <div class="form-group">
                        <input type="text" id="wsf_name0" name="wsf_name" placeholder="Имя" value=""
                               class="form-control">
                        <div id="wsf_error_name0" class="warning" style="display:none;"></div>
                      </div>
                      <div class="form-group">
                        <input type="tel" id="wsf_phone0" name="wsf_phone" placeholder="Телефон" value=""
                               class="form-control">
                        <div id="wsf_error_phone0" class="warning" style="display:none;"></div>
                      </div>
                      <div class="form-group text-center">
                        <a class="btn btn-default" id="feedback_submitter0"><span>Отправить</span></a>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div id="banner0">
              <div class="row">
                <div class="col-md-12 col-sm-4">
                  <div class="item">
                    <h3 class="banner-title">Популярное</h3>
                    <a href="<?php echo home_url(); ?>/populjarnye-rukzaki"><img
                        src="<?php echo get_template_directory_uri() ?>/img/popular-266x140_0.jpg"
                        alt="Популярное"
                        class="img-responsive center-block"></a>
                  </div>
                </div>
                <div class="col-md-12 col-sm-4">
                  <div class="item">
                    <h3 class="banner-title">Новинки</h3>
                    <a href="<?php echo home_url(); ?>/novinki-rjukzakov"><img
                        src="<?php echo get_template_directory_uri() ?>/img/latest-266x140_0.jpg"
                        alt="Новинки"
                        class="img-responsive center-block"></a>
                  </div>
                </div>
                <div class="col-md-12 col-sm-4">
                  <div class="item">
                    <h3 class="banner-title">Акции</h3>
                    <a href="<?php echo home_url(); ?>/special"><img
                        src="<?php echo get_template_directory_uri() ?>/img/special-266x140_0.jpg" alt="Акции"
                        class="img-responsive center-block"></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-9">
            <div id="slideshow0" class="owl-carousel">

              <div>
                <div class="item">
                  <h3 class="slider-title">Выбери свой рюкзак</h3>
                  <a href="<?php echo home_url(); ?>/categories"><img
                      src="<?php echo get_template_directory_uri() ?>/img/slide1main1-854x444_0.jpg"
                      alt="Выбери свой рюкзак"
                      class="img-responsive center-block"></a>
                </div>
              </div>

              <div>
                <div class="item">
                  <h3 class="slider-title">Женские рюкзаки</h3>
                  <a href="<?php echo home_url(); ?>/category/zhenskie-ryukzaki"><img
                      src="<?php echo get_template_directory_uri() ?>/img/slide2main2-854x444_0.jpg"
                      alt="Женские рюкзаки"
                      class="img-responsive center-block"></a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="widget">
          <h1 class="widget-heading">Интернет магазин рюкзаков №❶ Erukzak.com.ua</h1>
          <div class="widget-content">
            <div class="row">
              <div class="col-sm-4">
                <div class="category-item">
                  <a href="<?php echo home_url(); ?>/category/detckie-ryukzaki"><img
                      src="<?php echo get_template_directory_uri() ?>/img/ryukzaki-364x190_0.jpg" alt="Детские рюкзаки"
                      class="img-responsive center-block"></a>
                  <div class="name"><a href="<?php echo home_url(); ?>/category/detckie-ryukzaki">Детские рюкзаки</a></div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="category-item">
                  <a href="<?php echo home_url(); ?>/category/zhenskie-ryukzaki"><img
                      src="<?php echo get_template_directory_uri() ?>/img/zhenskie-rukzaki-364x190_0.jpg"
                      alt="Женские рюкзаки"
                      class="img-responsive center-block"></a>
                  <div class="name"><a href="<?php echo home_url(); ?>/category/zhenskie-ryukzaki">Женские рюкзаки</a>
                  </div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="category-item">
                  <a href="<?php echo home_url(); ?>/category/malenkie-ryukzaki"><img
                      src="<?php echo get_template_directory_uri() ?>/img/malenkie-rukzaki-364x190_0.jpg"
                      alt="Маленькие рюкзаки"
                      class="img-responsive center-block"></a>
                  <div class="name"><a href="<?php echo home_url(); ?>/category/malenkie-ryukzaki">Маленькие рюкзаки</a>
                  </div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="category-item">
                  <a href="<?php echo home_url(); ?>/category/modnye-ryukzaki"><img
                      src="<?php echo get_template_directory_uri() ?>/img/modnye-ryukzaki-364x190_0.jpg"
                      alt="Модные рюкзаки"
                      class="img-responsive center-block"></a>
                  <div class="name"><a href="<?php echo home_url(); ?>/category/modnye-ryukzaki">Модные рюкзаки</a></div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="category-item">
                  <a href="<?php echo home_url(); ?>/category/muzhskie-ryukzaki"><img
                      src="<?php echo get_template_directory_uri() ?>/img/muzhskie-rukz-364x190_0.jpg"
                      alt="Мужские рюкзаки"
                      class="img-responsive center-block"></a>
                  <div class="name"><a href="<?php echo home_url(); ?>/category/muzhskie-ryukzaki">Мужские рюкзаки</a>
                  </div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="category-item">
                  <a href="<?php echo home_url(); ?>/category/ryukzaki-dlya-podroctkov"><img
                      src="<?php echo get_template_directory_uri() ?>/img/rukzaki-dlya-podrostkov-364x190_0.jpg"
                      alt="Рюкзаки для подростков"
                      class="img-responsive center-block"></a>
                  <div class="name"><a href="<?php echo home_url(); ?>/category/ryukzaki-dlya-podroctkov">Рюкзаки для
                      подростков</a></div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="category-item">
                  <a href="<?php echo home_url(); ?>/category/ryukzaki-s-printami"><img
                      src="<?php echo get_template_directory_uri() ?>/img/rukzaki-s-printami-364x190_0.jpg"
                      alt="Рюкзаки с принтами"
                      class="img-responsive center-block"></a>
                  <div class="name"><a href="<?php echo home_url(); ?>/category/ryukzaki-s-printami">Рюкзаки с принтами</a>
                  </div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="category-item">
                  <a href="<?php echo home_url(); ?>/category/tkanevie-ryukzaki"><img
                      src="<?php echo get_template_directory_uri() ?>/img/tkanevye-rukzaki-364x190_0.jpg"
                      alt="Тканевые рюкзаки"
                      class="img-responsive center-block"></a>
                  <div class="name"><a href="<?php echo home_url(); ?>/category/tkanevie-ryukzaki">Тканевые рюкзаки</a>
                  </div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="category-item">
                  <a href="<?php echo home_url(); ?>/category/turisticheskie-ryukzaki"><img
                      src="<?php echo get_template_directory_uri() ?>/img/turisticheskie-rukzaki-364x190_0.jpg"
                      alt="Туристические рюкзаки"
                      class="img-responsive center-block"></a>
                  <div class="name"><a href="<?php echo home_url(); ?>/category/turisticheskie-ryukzaki">Туристические
                      рюкзаки</a></div>
                </div>
              </div>
            </div>

            <!--div class="buttons-block text-center">
				<a href="<?php echo home_url(); ?>/categories" class="btn btn-default btn-lg">Смотреть всю продукцию</a>
			</div-->
          </div>
        </div>
        <div class="text-block" id="text-block1">
          <div class="row">
            <div class="col-sm-8">
              <div class="text-content">
                <h2 class="text-heading h3">Где самые лучшие рюкзаки? Конечно, у нас!</h2>
                <p style="text-align: justify;">Комфортно в любой ситуации, или мы вернем вам деньги! Выбрать идеальный
                  рюкзак – задача не из легких, и первое, что для этого нужно, – огромный ассортимент! А какие
                  требования
                  вы предъявляете рюкзаку? Каково же его целевое предназначение? Он должен быть вместительным? Или вас
                  интересует качество? Может, просто достаточно прочности? Или вы хотите универсальный рюкзак, который
                  подойдет под любой образ и станет чем-то вроде стильного аксессуара? Вы уже определились с
                  функциональными характеристиками? Тогда можно считать, что вы на один шаг приблизились к тому, чтобы
                  купить рюкзак. Сегодня это – не просто обязательный предмет для любого туриста, а по-особенному
                  желанная
                  вещь для тех, кто ценит удобство. Самый обыкновенный заплечный мешок прошел довольно интересный
                  исторический путь, и теперь он представляет собой модный атрибут, который востребован как среди
                  мужчин,
                  так и среди представительниц прекрасного пола. Такую любовь к данному аксессуару можно объяснить,
                  прежде
                  всего, его конструктивными и функциональными особенностями. Не удивительно, что рюкзаки в Украине с
                  каждым днем пользуются все большим спросом, ведь это – чуть ли не самый удобный способ переносить
                  какие-либо вещи! Эта универсальная разновидность сумки может сослужить долгую службу, а самое
                  примечательное заключается в том, что какой бы впечатляющей вместительностью не располагал рюкзак, это
                  никак не повлияет на уровень комфорта! Удобство, как говорится, превыше всего! Теперь найти рюкзак,
                  соответствующий всем вашим потребностям и ожиданиям, стало еще проще! Наш интернет магазин рюкзаков
                  работает исключительно для вас! Широчайший ассортимент позволит найти именно то, что вы ищете.
                  Купленные
                  вещи должны приносить радость, не так ли? Мы гарантируем, что наши рюкзаки сделают вас счастливее! У
                  вас
                  есть шанс убедиться в этом! Сделать своего обладателя немного счастливее способны только любимые вещи…
                  А
                  мы точно знаем, что наши товары заслужат вашу любовь!</p>

              </div>
            </div>
          </div>
        </div>
        <div class="text-block" id="text-block2">
          <div class="row">
            <div class="col-sm-8 col-sm-offset-4">
              <div class="text-content">
                <h2 class="text-heading h3">В любой непонятной ситуации – загляни в свой рюкзак!</h2>
                <p style="text-align: justify;">Забыть наручные часы дома? Не страшно! Но вот забыть дома рюкзак –
                  лишить
                  себя комфорта на весь день. Эта разновидность сумки – настоящее чудо, вполне справедливо завоевавшее
                  доверие абсолютно каждого! Ищете школьный рюкзак для ребенка? Такой, чтоб и качественный, и
                  симпатичный,
                  и удобный, и вообще, чтобы на пару лет хватило? Мы предлагаем вам только такие модели, которые сами бы
                  купили своим детям! Хотите самый стильный рюкзак? Тканевые, с принтами, маленькие, большие, цветные…
                  Пожалуйста, остановите нас! Мы можем перечислять наш ассортимент очень долго! Конечно, самый большой
                  выбор любых товаров – в столице, а рюкзаки в городе Киев становятся настолько востребованными, что их
                  уже можно купить практически где угодно! Но что бы вы там не нашли, у нас вы найдете больше и
                  качественнее, а приятный бонус – выгодная стоимость. Как показывает практика, даже те покупатели,
                  которые точно знают, чего хотят, только в 30% случаев находят и покупают то, чего действительно
                  хотели.
                  Окажитесь в числе счастливчиков! Не мучайте себя походами по магазинам, не тратьте свое драгоценное
                  время – у нас есть все, чего только ваша душа пожелает! Теперь, если вы хотите купить рюкзак в Киеве,
                  нет необходимости в долгих поисках идеального места, где собраны лучшие товары. Все самые достойные
                  модели мы собрали в своем ассортименте. Даже не сомневайтесь. Своим товарам мы предъявляем самые
                  строгие
                  требования, тщательно подбираем каждую модель. Мы точно знаем, каким должен быть рюкзак самого
                  заядлого
                  путешественника, мы знаем, с каким бы рюкзаком хотела бы красоваться каждая девушка, или какие рюкзаки
                  могут носить деловые люди. Наши товары – превосходный симбиоз выгодной цены и безупречного качества –
                  об
                  этом уже знает вся Украина! Побалуйте себя по-настоящему качественной покупкой, которая непременно
                  станет одной из ваших любимых и незаменимых вещей.</p>

              </div>
            </div>
          </div>
        </div>
        <div class="widget">
          <div class="widget-heading">Отзывы</div>
          <div class="widget-content">
            <div class="row">
              <div class="col-md-6">
                <div class="testimonial-item">
                  <div class="row">
                    <div class="col-sm-4">
                      <div class="image">
                        <img src="<?php echo get_template_directory_uri() ?>/img/srg-200x200_0.png" alt="Сергей"
                             class="img-responsive center-block">
                        <span class="sprite backpack0"></span>
                      </div>
                    </div>
                    <div class="col-sm-8">
                      <div class="name">
                        Сергей , Черкасы
                      </div>
                      <div class="description">«Долго выбирая недорогой и в то же время качественный рюкзак, в Интернете
                        наткнулся на этот магазин. Порадовал его ассортимент и цены. Выбор рюкзаков – огромный. Там и
                        мужские, и женские, и детские...»
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="testimonial-item">
                  <div class="row">
                    <div class="col-sm-4">
                      <div class="image">
                        <img src="<?php echo get_template_directory_uri() ?>/img/igor-200x200_0.png" alt="Игорь"
                             class="img-responsive center-block">
                        <span class="sprite backpack1"></span>
                      </div>
                    </div>
                    <div class="col-sm-8">
                      <div class="name">
                        Игорь , Киев
                      </div>
                      <div class="description">«Что бы я ни покупал, в первую очередь обращаю внимание на производителя.
                        Не приемлю китайское. С рюкзаком та же ситуация. Хотелось найти что-то действительно
                        качественное
                        и модное, а выбор на рынк...»
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
              <div class="col-md-6">
                <div class="testimonial-item">
                  <div class="row">
                    <div class="col-sm-4">
                      <div class="image">
                        <img src="<?php echo get_template_directory_uri() ?>/img/elena-200x200_0.png" alt="Елена"
                             class="img-responsive center-block">
                        <span class="sprite backpack2"></span>
                      </div>
                    </div>
                    <div class="col-sm-8">
                      <div class="name">
                        Елена , Кировоград
                      </div>
                      <div class="description">«Покупала здесь рюкзак дочке в школу. Она, как и все дети, хотела что-то
                        яркое, красивое, не такое, как у всех. А для меня, в первую очередь, было важно качество и
                        практичность. Когда зашли в разде...»
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="testimonial-item">
                  <div class="row">
                    <div class="col-sm-4">
                      <div class="image">
                        <img src="<?php echo get_template_directory_uri() ?>/img/milana-200x200_0.png" alt="Милана"
                             class="img-responsive center-block">
                        <span class="sprite backpack3"></span>
                      </div>
                    </div>
                    <div class="col-sm-8">
                      <div class="name">
                        Милана , Шостка
                      </div>
                      <div class="description">«Чтобы в наше время купить хороший рюкзак, надо выкинуть немаленькую
                        сумму.
                        Поэтому я долго выбирала, где можно приобрести вещь нормального качества подешевле. Хорошо, что
                        нашла этот интернет-супер...»
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="buttons-block text-center">
              <a href="<?php echo home_url(); ?>/testimonial" class="btn btn-default btn-lg">Написать отзыв</a>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-4 col-sm-offset-7">

            <div class="map-wrapper">
              <div class="map-content">
                <h3>Украина, Киев</h3>

                <p>г. Киев, пр. Победы 27<br>
                  тел.: (044) 232-66-32, (063) 065-55-00, (067) 448 23 71<br>
                  e-mail: erukzak@gmail.com<br>
                  skype: erukzak.com.ua</p>

              </div>
            </div>
          </div>
        </div>
        <div class="widget">
          <div class="widget-content">
			  <?php echo do_shortcode( '[google_map_easy id="1"]' ) ?>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php get_footer();