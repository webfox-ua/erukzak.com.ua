<?php get_header() ?>
    <div class="container">
        <div class="row">
            <div id="content" class="col-md-12">
                <div class="not-found-content">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/error.png" alt="error">
                    <h1><?php esc_html_e('К сожалению, запрошенная Вами страница не существует','erukzak'); ?></h1>
                    <p><?php esc_html_e('Возможно она была удалена либо вы набрали неверный адрес.','erukzak'); ?></p>
                    <p><a href="<?php echo home_url(); ?>" class="btn btn-primary btn-lg"><?php esc_html_e( 'На главную', 'erukzak' ) ?></a></p>
                </div>
            </div>
        </div>
    </div>
<?php get_footer();